﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using FlexCodeSDK;
using System.IO;
using System.Text.RegularExpressions;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class CandidateRegistration : Form
    {
        FlexCodeSDK.FinFPReg fpreg;
        String template = "";
        string imgPath = "";

        public CandidateRegistration()
        {
            InitializeComponent();
        }
        MySqlConnection Connection = new MySqlConnection("server=localhost;database=icbtelection;username=root;password=;");
        MySqlCommand Command;
        void reg_FPRegistrationStatus(RegistrationStatus Status)
        {
            if (Status == RegistrationStatus.r_OK)
            {
                MemoryStream ms = new MemoryStream();
                pictureBoxCandidate.Image.Save(ms, pictureBoxCandidate.Image.RawFormat);
                byte[] img = ms.ToArray();

                MySqlParameter[] parameters = new MySqlParameter[12];

                parameters[0] = new MySqlParameter("CID", MySqlDbType.VarChar, 20);
                parameters[0].Value = textBoxCID.Text;

                parameters[1] = new MySqlParameter("CFname", MySqlDbType.VarChar, 50);
                parameters[1].Value = textBoxCFname.Text;

                parameters[2] = new MySqlParameter("CGender", MySqlDbType.VarChar, 20);
                parameters[2].Value = comboBoxCGender.Text;

                parameters[3] = new MySqlParameter("CNIC", MySqlDbType.VarChar, 10);
                parameters[3].Value = textBoxCNIC.Text;

                parameters[4] = new MySqlParameter("CEmail", MySqlDbType.VarChar, 50);
                parameters[4].Value = textBoxCEmail.Text;

                parameters[5] = new MySqlParameter("CContNo", MySqlDbType.VarChar, 10);
                parameters[5].Value = textBoxCContNo.Text;

                parameters[6] = new MySqlParameter("Image", MySqlDbType.LongBlob);
                parameters[6].Value = img;

                parameters[7] = new MySqlParameter("Post", MySqlDbType.VarChar, 20);
                parameters[7].Value = comboBoxPost.Text;

                parameters[8] = new MySqlParameter("PID", MySqlDbType.VarChar, 20);
                parameters[8].Value = textBoxPID.Text;

                parameters[9] = new MySqlParameter("Pname", MySqlDbType.VarChar, 50);
                parameters[9].Value = textBoxPname.Text;

                parameters[10] = new MySqlParameter("Username", MySqlDbType.VarChar, 20);
                parameters[10].Value = textBoxCUname.Text;

                parameters[11] = new MySqlParameter("Templete", MySqlDbType.VarChar, 50);
                parameters[11].Value = template;
               
                Command = new MySqlCommand();
                Command.CommandType = CommandType.StoredProcedure;
                Command.CommandText = "Add_Candidate";
                Command.Connection = Connection;
                Command.Parameters.AddRange(parameters);
                Command.ExecuteNonQuery();
                MessageBox.Show("Candidate Registered successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
               
                //clear textboxes
                textBoxCID.Text = String.Empty;
                textBoxCFname.Text = String.Empty;
                comboBoxCGender.Text = String.Empty;
                textBoxCNIC.Text = String.Empty;
                textBoxCEmail.Text = String.Empty;
                textBoxCContNo.Text = String.Empty;
                textBoxPID.Text = String.Empty;
                textBoxPname.Text = String.Empty;
                textBoxCUname.Text = String.Empty;
                comboBoxPost.Text = String.Empty;
                pictureBox1.Image = null;
                pictureBoxCandidate.Image = null;
                
                //close the connection
                Connection.Close();

            }
        }



        void reg_FPRegistrationTemplate(string FPTemplate)
        {
            template = FPTemplate;
        }

        void reg_FPRegistrationImage()
        {
            pictureBox1.Load(imgPath);
            if (imgPath == "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\103_1.tif")
            {
                imgPath = "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\103_2.tif";
            }
            else
            {
                imgPath = "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\103_1.tif";
            }
            fpreg.PictureSamplePath = imgPath;
        }

        void reg_FPSamplesNeeded(short Samples)
        {
            label5.Text = "Samples Needed : " + Convert.ToString(Samples);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            AdminPanel obj = new AdminPanel();
            this.Hide();
            obj.Show();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxCID.Text = String.Empty;
            textBoxCFname.Text = String.Empty;
            comboBoxCGender.Text = String.Empty;
            textBoxCNIC.Text = String.Empty;
            textBoxCEmail.Text = String.Empty;
            textBoxCContNo.Text = String.Empty;
            textBoxPID.Text = String.Empty;
            textBoxPname.Text = String.Empty;
            textBoxCUname.Text = String.Empty;
            comboBoxPost.Text = String.Empty;
            pictureBox1.Image = null;
            pictureBoxCandidate.Image = null;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void labelVID_Click(object sender, EventArgs e)
        {

        }

        private void labelFname_Click(object sender, EventArgs e)
        {

        }

        private void labelLName_Click(object sender, EventArgs e)
        {

        }

        private void labelGender_Click(object sender, EventArgs e)
        {

        }

        private void CandidateRegistration_Load(object sender, EventArgs e)
        {
         
            Connection.Open();

            //Initialize FlexCodeSDK for Registration
            //1. Initialize Event Handler
            fpreg = new FlexCodeSDK.FinFPReg();
            fpreg.FPSamplesNeeded += new __FinFPReg_FPSamplesNeededEventHandler(reg_FPSamplesNeeded);
            fpreg.FPRegistrationTemplate += new __FinFPReg_FPRegistrationTemplateEventHandler(reg_FPRegistrationTemplate);
            fpreg.FPRegistrationImage += new __FinFPReg_FPRegistrationImageEventHandler(reg_FPRegistrationImage);
            fpreg.FPRegistrationStatus += new __FinFPReg_FPRegistrationStatusEventHandler(reg_FPRegistrationStatus);

            //2. Input the activation code
            fpreg.AddDeviceInfo("44037546", "FE8C5A406D7F6DB", "ALD5BB724DF16EACB38D1YB4");

            //3. Define fingerprint image
            fpreg.PictureSampleHeight = (short)(pictureBox1.Height * 20); //FlexCodeSDK use Twips. 1 pixel = 15 twips
            fpreg.PictureSampleWidth = (short)(pictureBox1.Width * 20); //FlexCodeSDK use Twips. 1 pixel = 15 twips
            imgPath = "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\103_1.tif";
            fpreg.PictureSamplePath = imgPath;
        }

        private void labelEmail_Click(object sender, EventArgs e)
        {

        }

        private void labelNIC_Click(object sender, EventArgs e)
        {

        }

        private void labelContactno_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private bool ValidateNIC(string p)
        {
            bool isMatchCase = Regex.IsMatch(p, @"^\d{9}(x|X|v|V)$");
            return isMatchCase;
        }

        private bool ValidateEmail(string w)
        {
            bool isMatchCase = Regex.IsMatch(w, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            return isMatchCase;
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
             if (textBoxCID.Text == "")/*To check whether fields are empty*/
            {
                MessageBox.Show("Enter the Candidate ID", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxCID.Focus();
            }
            else if (textBoxCFname.Text == "")
            {
                MessageBox.Show("Enter the First Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxCFname.Focus();
            }
         
             else if (comboBoxCGender.Text == "")
             {
                 MessageBox.Show("Select the Gender", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
             }
             else if (textBoxCNIC.Text == "")
             {
                 MessageBox.Show("Enter the NIC No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxCNIC.Focus();
             }
             else if (textBoxCNIC.Text.Length!=10)
             {
                 MessageBox.Show("Use 10 characters for NIC No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxCNIC.Text = String.Empty;
                 textBoxCNIC.Focus();
             }
             else if (ValidateNIC(textBoxCNIC.Text) == false)
             {
                 MessageBox.Show("Invalid NIC Format", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxCNIC.Text = String.Empty;
                 textBoxCNIC.Focus();
             }
             else if (textBoxCEmail.Text == "")
             {
                 MessageBox.Show("Enter the Email", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxCEmail.Focus();
             }
             else if (ValidateEmail(textBoxCEmail.Text) == false)
             {
                 MessageBox.Show("Invalid Email Format", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxCEmail.Text = String.Empty;
                 textBoxCEmail.Focus();

             }
             else if (textBoxCContNo.Text == "")
             {
                 MessageBox.Show("Enter the Contact No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxCContNo.Focus();
             }
             else if (textBoxCContNo.Text.Length != 10)
             {
                 MessageBox.Show("Use 10 characters for Contact No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxCContNo.Text = String.Empty;
                 textBoxCContNo.Focus();
             }
             else if (System.Text.RegularExpressions.Regex.IsMatch(textBoxCContNo.Text, "[^0-9]"))
             {
                 MessageBox.Show("Invalid contact No Format", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxCContNo.Text = String.Empty;
                 textBoxCContNo.Focus();
             }
             else if (textBoxPID.Text == "")
             {
                 MessageBox.Show("Enter the Party ID", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxPID.Focus();
             }
             else if (textBoxPname.Text == "")
             {
                 MessageBox.Show("Enter the Party Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxPname.Focus();
             }
             else if (textBoxCUname.Text == "")
             {
                 MessageBox.Show("Enter the User Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxCUname.Focus();
             }
             else if (comboBoxPost.Text == "")
             {
                 MessageBox.Show("Select the Post", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
             }

             else
             {
                 DBLayer.DBConnection obj1 = new DBLayer.DBConnection();
                 MySqlCommand da = new MySqlCommand("select count(*)from candidate where CID='" + textBoxCID.Text + "'", obj1.createConnection());
                 int count = Convert.ToInt32(da.ExecuteScalar());


                 if (count > 0)
                 {
                     MessageBox.Show("This Candidate ID is already in the database. Please use another ID !!", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 }

                 else
                 {
                     fpreg.FPRegistrationStart("MySecretKey" + textBoxCID.Text);
                 }
             }
        }

        private void buttonSelectSymbol_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();

            opf.Filter = "jpg files (*.jpg)|*.jpg";
            if (opf.ShowDialog() == DialogResult.OK)
            {
                pictureBoxCandidate.Image = System.Drawing.Image.FromFile(opf.FileName);
            }
            opf.Dispose();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBoxCUname_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxPost_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace PatternRecognition.FingerprintRecognition.Applications
{
    partial class AdminPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminPanel));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.voterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voterToolStripMenuRegvoter = new System.Windows.Forms.ToolStripMenuItem();
            this.updateRegistrationInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.candidateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.candidateRegistrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateCandidateInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partyRegistrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updatePartyRegistrationInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registerAdministratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateAdministratorInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.votesCalculationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateCandidateVotesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculatePartyVotesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.DarkTurquoise;
            this.menuStrip1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.voterToolStripMenuItem,
            this.candidateToolStripMenuItem,
            this.partyToolStripMenuItem,
            this.administratorToolStripMenuItem,
            this.votesCalculationToolStripMenuItem,
            this.logOutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1370, 27);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // voterToolStripMenuItem
            // 
            this.voterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.voterToolStripMenuRegvoter,
            this.updateRegistrationInformationToolStripMenuItem});
            this.voterToolStripMenuItem.Name = "voterToolStripMenuItem";
            this.voterToolStripMenuItem.Size = new System.Drawing.Size(58, 23);
            this.voterToolStripMenuItem.Text = "Voter";
            // 
            // voterToolStripMenuRegvoter
            // 
            this.voterToolStripMenuRegvoter.BackColor = System.Drawing.Color.DarkTurquoise;
            this.voterToolStripMenuRegvoter.Name = "voterToolStripMenuRegvoter";
            this.voterToolStripMenuRegvoter.Size = new System.Drawing.Size(227, 24);
            this.voterToolStripMenuRegvoter.Text = "Voter Registration";
            this.voterToolStripMenuRegvoter.Click += new System.EventHandler(this.voterToolStripMenuItem1_Click);
            // 
            // updateRegistrationInformationToolStripMenuItem
            // 
            this.updateRegistrationInformationToolStripMenuItem.BackColor = System.Drawing.Color.DarkTurquoise;
            this.updateRegistrationInformationToolStripMenuItem.Name = "updateRegistrationInformationToolStripMenuItem";
            this.updateRegistrationInformationToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.updateRegistrationInformationToolStripMenuItem.Text = "Update/Remove Voter";
            this.updateRegistrationInformationToolStripMenuItem.Click += new System.EventHandler(this.updateRegistrationInformationToolStripMenuItem_Click);
            // 
            // candidateToolStripMenuItem
            // 
            this.candidateToolStripMenuItem.BackColor = System.Drawing.Color.DarkTurquoise;
            this.candidateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.candidateRegistrationToolStripMenuItem,
            this.updateCandidateInformationToolStripMenuItem});
            this.candidateToolStripMenuItem.Name = "candidateToolStripMenuItem";
            this.candidateToolStripMenuItem.Size = new System.Drawing.Size(89, 23);
            this.candidateToolStripMenuItem.Text = "Candidate";
            // 
            // candidateRegistrationToolStripMenuItem
            // 
            this.candidateRegistrationToolStripMenuItem.BackColor = System.Drawing.Color.DarkTurquoise;
            this.candidateRegistrationToolStripMenuItem.Name = "candidateRegistrationToolStripMenuItem";
            this.candidateRegistrationToolStripMenuItem.Size = new System.Drawing.Size(258, 24);
            this.candidateRegistrationToolStripMenuItem.Text = "Candidate Registration";
            this.candidateRegistrationToolStripMenuItem.Click += new System.EventHandler(this.candidateRegistrationToolStripMenuItem_Click);
            // 
            // updateCandidateInformationToolStripMenuItem
            // 
            this.updateCandidateInformationToolStripMenuItem.BackColor = System.Drawing.Color.DarkTurquoise;
            this.updateCandidateInformationToolStripMenuItem.Name = "updateCandidateInformationToolStripMenuItem";
            this.updateCandidateInformationToolStripMenuItem.Size = new System.Drawing.Size(258, 24);
            this.updateCandidateInformationToolStripMenuItem.Text = "Update/Remove Candidate";
            this.updateCandidateInformationToolStripMenuItem.Click += new System.EventHandler(this.updateCandidateInformationToolStripMenuItem_Click);
            // 
            // partyToolStripMenuItem
            // 
            this.partyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.partyRegistrationToolStripMenuItem,
            this.updatePartyRegistrationInformationToolStripMenuItem});
            this.partyToolStripMenuItem.Name = "partyToolStripMenuItem";
            this.partyToolStripMenuItem.Size = new System.Drawing.Size(57, 23);
            this.partyToolStripMenuItem.Text = "Party";
            // 
            // partyRegistrationToolStripMenuItem
            // 
            this.partyRegistrationToolStripMenuItem.BackColor = System.Drawing.Color.DarkTurquoise;
            this.partyRegistrationToolStripMenuItem.Name = "partyRegistrationToolStripMenuItem";
            this.partyRegistrationToolStripMenuItem.Size = new System.Drawing.Size(226, 24);
            this.partyRegistrationToolStripMenuItem.Text = "Party Registration";
            this.partyRegistrationToolStripMenuItem.Click += new System.EventHandler(this.partyRegistrationToolStripMenuItem_Click);
            // 
            // updatePartyRegistrationInformationToolStripMenuItem
            // 
            this.updatePartyRegistrationInformationToolStripMenuItem.BackColor = System.Drawing.Color.DarkTurquoise;
            this.updatePartyRegistrationInformationToolStripMenuItem.Name = "updatePartyRegistrationInformationToolStripMenuItem";
            this.updatePartyRegistrationInformationToolStripMenuItem.Size = new System.Drawing.Size(226, 24);
            this.updatePartyRegistrationInformationToolStripMenuItem.Text = "Update/Remove Party";
            this.updatePartyRegistrationInformationToolStripMenuItem.Click += new System.EventHandler(this.updatePartyRegistrationInformationToolStripMenuItem_Click);
            // 
            // administratorToolStripMenuItem
            // 
            this.administratorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registerAdministratorToolStripMenuItem,
            this.updateAdministratorInformationToolStripMenuItem});
            this.administratorToolStripMenuItem.Name = "administratorToolStripMenuItem";
            this.administratorToolStripMenuItem.Size = new System.Drawing.Size(113, 23);
            this.administratorToolStripMenuItem.Text = "Administrator";
            // 
            // registerAdministratorToolStripMenuItem
            // 
            this.registerAdministratorToolStripMenuItem.BackColor = System.Drawing.Color.DarkTurquoise;
            this.registerAdministratorToolStripMenuItem.Name = "registerAdministratorToolStripMenuItem";
            this.registerAdministratorToolStripMenuItem.Size = new System.Drawing.Size(281, 24);
            this.registerAdministratorToolStripMenuItem.Text = "Register Administrator";
            this.registerAdministratorToolStripMenuItem.Click += new System.EventHandler(this.registerAdministratorToolStripMenuItem_Click);
            // 
            // updateAdministratorInformationToolStripMenuItem
            // 
            this.updateAdministratorInformationToolStripMenuItem.BackColor = System.Drawing.Color.DarkTurquoise;
            this.updateAdministratorInformationToolStripMenuItem.Name = "updateAdministratorInformationToolStripMenuItem";
            this.updateAdministratorInformationToolStripMenuItem.Size = new System.Drawing.Size(281, 24);
            this.updateAdministratorInformationToolStripMenuItem.Text = "Update/Remove Administrator";
            this.updateAdministratorInformationToolStripMenuItem.Click += new System.EventHandler(this.updateAdministratorInformationToolStripMenuItem_Click);
            // 
            // votesCalculationToolStripMenuItem
            // 
            this.votesCalculationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculateCandidateVotesToolStripMenuItem,
            this.calculatePartyVotesToolStripMenuItem});
            this.votesCalculationToolStripMenuItem.Name = "votesCalculationToolStripMenuItem";
            this.votesCalculationToolStripMenuItem.Size = new System.Drawing.Size(138, 23);
            this.votesCalculationToolStripMenuItem.Text = "Votes Calculation";
            // 
            // calculateCandidateVotesToolStripMenuItem
            // 
            this.calculateCandidateVotesToolStripMenuItem.BackColor = System.Drawing.Color.DarkTurquoise;
            this.calculateCandidateVotesToolStripMenuItem.Name = "calculateCandidateVotesToolStripMenuItem";
            this.calculateCandidateVotesToolStripMenuItem.Size = new System.Drawing.Size(255, 24);
            this.calculateCandidateVotesToolStripMenuItem.Text = "Calculate Candidate Votes";
            this.calculateCandidateVotesToolStripMenuItem.Click += new System.EventHandler(this.calculateCandidateVotesToolStripMenuItem_Click);
            // 
            // calculatePartyVotesToolStripMenuItem
            // 
            this.calculatePartyVotesToolStripMenuItem.BackColor = System.Drawing.Color.DarkTurquoise;
            this.calculatePartyVotesToolStripMenuItem.Name = "calculatePartyVotesToolStripMenuItem";
            this.calculatePartyVotesToolStripMenuItem.Size = new System.Drawing.Size(255, 24);
            this.calculatePartyVotesToolStripMenuItem.Text = "Calculate Party Votes";
            this.calculatePartyVotesToolStripMenuItem.Click += new System.EventHandler(this.calculatePartyVotesToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(72, 23);
            this.logOutToolStripMenuItem.Text = "LogOut";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Cyan;
            this.label1.Location = new System.Drawing.Point(162, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1054, 465);
            this.label1.TabIndex = 1;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Aqua;
            this.label6.Location = new System.Drawing.Point(558, 666);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(223, 15);
            this.label6.TabIndex = 156;
            this.label6.Text = "ICBT Campus @ All Rights Reservered";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.client_img_1;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(-2, 21);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(277, 94);
            this.pictureBox3.TabIndex = 169;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.ICBTkandy;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(272, 21);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1098, 94);
            this.pictureBox2.TabIndex = 170;
            this.pictureBox2.TabStop = false;
            // 
            // AdminPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources._3d_desktop_technology_wallpaper_backgrounds_for_download1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1370, 750);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AdminPanel";
            this.Text = "AdminPanel";
            this.Load += new System.EventHandler(this.AdminPanel_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem voterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voterToolStripMenuRegvoter;
        private System.Windows.Forms.ToolStripMenuItem updateRegistrationInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem candidateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem candidateRegistrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateCandidateInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem partyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem partyRegistrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updatePartyRegistrationInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administratorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registerAdministratorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateAdministratorInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem votesCalculationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculateCandidateVotesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculatePartyVotesToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;

    }
}
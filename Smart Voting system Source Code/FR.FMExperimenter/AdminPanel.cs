﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class AdminPanel : Form
    {
        public AdminPanel()
        {
            InitializeComponent();
        }

        private void registrationToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void updateInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void voterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            VoterRegistration obj = new VoterRegistration();
            this.Hide();
            obj.Show();

        }

        private void partyRegistrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PartyRegistration obj = new PartyRegistration();
            this.Hide();
            obj.Show();

        }

        private void updateRegistrationInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateVoter obj = new UpdateVoter();
            this.Hide();
            obj.Show();
        }

        private void candidateRegistrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CandidateRegistration obj = new CandidateRegistration();
            this.Hide();
            obj.Show();
        }

        private void updateCandidateInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateCandidate obj = new UpdateCandidate();
            this.Hide();
            obj.Show();
        }

        private void updatePartyRegistrationInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateParty obj = new UpdateParty();
            this.Hide();
            obj.Show();
        }

        private void AdminPanel_Load(object sender, EventArgs e)
        {

        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Welcome  obj = new Welcome();
            this.Hide();
            obj.Show();
        }

        private void registerAdministratorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminRegistration obj = new AdminRegistration();
            this.Hide();
            obj.Show();
        }

        private void updateAdministratorInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateAdmin obj = new UpdateAdmin();
            this.Hide();
            obj.Show();
        }

        private void calculateCandidateVotesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CalculateCandidateVotes obj = new CalculateCandidateVotes();
            this.Hide();
            obj.Show();
        }

        private void calculatePartyVotesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CalculatePartyVotes obj = new CalculatePartyVotes();
            this.Hide();
            obj.Show();
        }
    }
}

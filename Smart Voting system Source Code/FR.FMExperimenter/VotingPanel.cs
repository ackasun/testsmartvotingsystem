﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class VotingPanel : Form
    {
        public VotingPanel()
        {
            InitializeComponent();
        }

        private void comboBoxPname_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void VotingPanel_Load(object sender, EventArgs e)
        {
            try
            {
                MySqlConnection mcon = new MySqlConnection(@"server=localhost;database=icbtelection;username=root;password=;");
                string s = "SELECT * from candidate WHERE Post='Prasident'";
                mcon.Open();
                MySqlCommand mcd = new MySqlCommand(s,mcon);
                MySqlDataReader mdr = mcd.ExecuteReader();
                while (mdr.Read())
                {
                    comboBoxPname.Items.Add(mdr.GetString("Pname"));
                    comboBoxCandidateName.Items.Add(mdr.GetString("CFname"));
                }
                mcon.Close();

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonCastVote_Click(object sender, EventArgs e)
        {
            DBLayer.Vote obj = new DBLayer.Vote();

            obj.Name1 = comboBoxCandidateName.SelectedItem.ToString();



            obj.UpdateVotes(obj);
            //output message if update is successfull
            MessageBox.Show("Successfully Voted", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}

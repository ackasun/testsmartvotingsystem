﻿namespace PatternRecognition.FingerprintRecognition.Applications
{
    partial class VotingPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelVID = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxPname = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxCandidateName = new System.Windows.Forms.ComboBox();
            this.buttonCastVote = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.ICBTkandy;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(233, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(559, 94);
            this.pictureBox2.TabIndex = 124;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.client_img_1;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(-2, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(236, 94);
            this.pictureBox1.TabIndex = 123;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(256, 658);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(223, 15);
            this.label6.TabIndex = 122;
            this.label6.Text = "ICBT Campus @ All Rights Reservered";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(39, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(686, 55);
            this.label1.TabIndex = 121;
            this.label1.Text = "Cast the Vote for following Posts";
            // 
            // labelVID
            // 
            this.labelVID.AutoSize = true;
            this.labelVID.BackColor = System.Drawing.Color.Transparent;
            this.labelVID.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVID.ForeColor = System.Drawing.Color.Black;
            this.labelVID.Location = new System.Drawing.Point(45, 173);
            this.labelVID.Name = "labelVID";
            this.labelVID.Size = new System.Drawing.Size(88, 22);
            this.labelVID.TabIndex = 125;
            this.labelVID.Text = "Prasident";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(45, 218);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 19);
            this.label2.TabIndex = 126;
            this.label2.Text = "Party Name";
            // 
            // comboBoxPname
            // 
            this.comboBoxPname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPname.FormattingEnabled = true;
            this.comboBoxPname.Location = new System.Drawing.Point(158, 214);
            this.comboBoxPname.Name = "comboBoxPname";
            this.comboBoxPname.Size = new System.Drawing.Size(145, 27);
            this.comboBoxPname.TabIndex = 127;
            this.comboBoxPname.SelectedIndexChanged += new System.EventHandler(this.comboBoxPname_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(338, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 19);
            this.label3.TabIndex = 128;
            this.label3.Text = "Candidate Name";
            // 
            // comboBoxCandidateName
            // 
            this.comboBoxCandidateName.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCandidateName.FormattingEnabled = true;
            this.comboBoxCandidateName.Location = new System.Drawing.Point(486, 214);
            this.comboBoxCandidateName.Name = "comboBoxCandidateName";
            this.comboBoxCandidateName.Size = new System.Drawing.Size(121, 27);
            this.comboBoxCandidateName.TabIndex = 129;
            // 
            // buttonCastVote
            // 
            this.buttonCastVote.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCastVote.Location = new System.Drawing.Point(626, 214);
            this.buttonCastVote.Name = "buttonCastVote";
            this.buttonCastVote.Size = new System.Drawing.Size(137, 26);
            this.buttonCastVote.TabIndex = 130;
            this.buttonCastVote.Text = "Cast Vote";
            this.buttonCastVote.UseVisualStyleBackColor = true;
            this.buttonCastVote.Click += new System.EventHandler(this.buttonCastVote_Click);
            // 
            // VotingPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 682);
            this.Controls.Add(this.buttonCastVote);
            this.Controls.Add(this.comboBoxCandidateName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxPname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelVID);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Name = "VotingPanel";
            this.Text = "VotingPanel";
            this.Load += new System.EventHandler(this.VotingPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelVID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxPname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxCandidateName;
        private System.Windows.Forms.Button buttonCastVote;
    }
}
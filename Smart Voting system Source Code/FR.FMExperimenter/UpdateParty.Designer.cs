﻿namespace PatternRecognition.FingerprintRecognition.Applications
{
    partial class UpdateParty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBack = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.comboBoxNoCandi = new System.Windows.Forms.ComboBox();
            this.textBoxLeader = new System.Windows.Forms.TextBox();
            this.textBoxPname = new System.Windows.Forms.TextBox();
            this.labelGender = new System.Windows.Forms.Label();
            this.labelFname = new System.Windows.Forms.Label();
            this.labelLName = new System.Windows.Forms.Label();
            this.labelPID = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.comboBoxPID = new System.Windows.Forms.ComboBox();
            this.buttonSelectSymbol = new System.Windows.Forms.Button();
            this.pictureBoxSymbol = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSymbol)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonBack
            // 
            this.buttonBack.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBack.Location = new System.Drawing.Point(915, 566);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(178, 51);
            this.buttonBack.TabIndex = 71;
            this.buttonBack.Text = "Back";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdate.Location = new System.Drawing.Point(216, 566);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(178, 51);
            this.buttonUpdate.TabIndex = 70;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // comboBoxNoCandi
            // 
            this.comboBoxNoCandi.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxNoCandi.FormattingEnabled = true;
            this.comboBoxNoCandi.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10"});
            this.comboBoxNoCandi.Location = new System.Drawing.Point(543, 360);
            this.comboBoxNoCandi.Name = "comboBoxNoCandi";
            this.comboBoxNoCandi.Size = new System.Drawing.Size(148, 27);
            this.comboBoxNoCandi.TabIndex = 68;
            this.comboBoxNoCandi.Text = "Select No";
            this.comboBoxNoCandi.SelectedIndexChanged += new System.EventHandler(this.comboBoxNoCandi_SelectedIndexChanged);
            // 
            // textBoxLeader
            // 
            this.textBoxLeader.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLeader.Location = new System.Drawing.Point(543, 307);
            this.textBoxLeader.Name = "textBoxLeader";
            this.textBoxLeader.Size = new System.Drawing.Size(231, 26);
            this.textBoxLeader.TabIndex = 67;
            this.textBoxLeader.TextChanged += new System.EventHandler(this.textBoxLeader_TextChanged);
            // 
            // textBoxPname
            // 
            this.textBoxPname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPname.Location = new System.Drawing.Point(543, 252);
            this.textBoxPname.Name = "textBoxPname";
            this.textBoxPname.Size = new System.Drawing.Size(231, 26);
            this.textBoxPname.TabIndex = 66;
            this.textBoxPname.TextChanged += new System.EventHandler(this.textBoxPname_TextChanged);
            // 
            // labelGender
            // 
            this.labelGender.AutoSize = true;
            this.labelGender.BackColor = System.Drawing.Color.Transparent;
            this.labelGender.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGender.ForeColor = System.Drawing.Color.Aqua;
            this.labelGender.Location = new System.Drawing.Point(214, 356);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(152, 22);
            this.labelGender.TabIndex = 65;
            this.labelGender.Text = "No of Candidates";
            this.labelGender.Click += new System.EventHandler(this.labelGender_Click);
            // 
            // labelFname
            // 
            this.labelFname.AutoSize = true;
            this.labelFname.BackColor = System.Drawing.Color.Transparent;
            this.labelFname.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFname.ForeColor = System.Drawing.Color.Aqua;
            this.labelFname.Location = new System.Drawing.Point(214, 248);
            this.labelFname.Name = "labelFname";
            this.labelFname.Size = new System.Drawing.Size(108, 22);
            this.labelFname.TabIndex = 64;
            this.labelFname.Text = "Party Name";
            this.labelFname.Click += new System.EventHandler(this.labelFname_Click);
            // 
            // labelLName
            // 
            this.labelLName.AutoSize = true;
            this.labelLName.BackColor = System.Drawing.Color.Transparent;
            this.labelLName.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLName.ForeColor = System.Drawing.Color.Aqua;
            this.labelLName.Location = new System.Drawing.Point(214, 303);
            this.labelLName.Name = "labelLName";
            this.labelLName.Size = new System.Drawing.Size(118, 22);
            this.labelLName.TabIndex = 63;
            this.labelLName.Text = "Party Leader";
            this.labelLName.Click += new System.EventHandler(this.labelLName_Click);
            // 
            // labelPID
            // 
            this.labelPID.AutoSize = true;
            this.labelPID.BackColor = System.Drawing.Color.Transparent;
            this.labelPID.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPID.ForeColor = System.Drawing.Color.Aqua;
            this.labelPID.Location = new System.Drawing.Point(214, 204);
            this.labelPID.Name = "labelPID";
            this.labelPID.Size = new System.Drawing.Size(81, 22);
            this.labelPID.TabIndex = 61;
            this.labelPID.Text = "Party ID";
            this.labelPID.Click += new System.EventHandler(this.labelPID_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Aqua;
            this.label6.Location = new System.Drawing.Point(558, 666);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(223, 15);
            this.label6.TabIndex = 60;
            this.label6.Text = "ICBT Campus @ All Rights Reservered";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Aqua;
            this.label1.Location = new System.Drawing.Point(428, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(491, 55);
            this.label1.TabIndex = 59;
            this.label1.Text = "Update/Remove Party ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // buttonClear
            // 
            this.buttonClear.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClear.Location = new System.Drawing.Point(683, 566);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(178, 51);
            this.buttonClear.TabIndex = 91;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRemove.Location = new System.Drawing.Point(449, 566);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(178, 51);
            this.buttonRemove.TabIndex = 98;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.ICBTkandy;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(273, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1098, 94);
            this.pictureBox2.TabIndex = 105;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.client_img_1;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(277, 94);
            this.pictureBox3.TabIndex = 104;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.back;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(925, 576);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(34, 31);
            this.pictureBox6.TabIndex = 112;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.kripto_clear_b;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(695, 576);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(34, 31);
            this.pictureBox5.TabIndex = 111;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.editor_trash_delete_recycle_bin__512;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(460, 576);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(34, 31);
            this.pictureBox1.TabIndex = 141;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.refresh_512;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(229, 576);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(34, 31);
            this.pictureBox4.TabIndex = 140;
            this.pictureBox4.TabStop = false;
            // 
            // comboBoxPID
            // 
            this.comboBoxPID.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPID.FormattingEnabled = true;
            this.comboBoxPID.Items.AddRange(new object[] {
            "Select Party ID"});
            this.comboBoxPID.Location = new System.Drawing.Point(543, 198);
            this.comboBoxPID.Name = "comboBoxPID";
            this.comboBoxPID.Size = new System.Drawing.Size(133, 27);
            this.comboBoxPID.TabIndex = 142;
            this.comboBoxPID.Text = "Select Party ID";
            this.comboBoxPID.SelectedIndexChanged += new System.EventHandler(this.comboBoxPID_SelectedIndexChanged);
            // 
            // buttonSelectSymbol
            // 
            this.buttonSelectSymbol.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectSymbol.Location = new System.Drawing.Point(894, 384);
            this.buttonSelectSymbol.Name = "buttonSelectSymbol";
            this.buttonSelectSymbol.Size = new System.Drawing.Size(199, 40);
            this.buttonSelectSymbol.TabIndex = 144;
            this.buttonSelectSymbol.Text = "Select Symbol";
            this.buttonSelectSymbol.UseVisualStyleBackColor = true;
            this.buttonSelectSymbol.Click += new System.EventHandler(this.buttonSelectSymbol_Click);
            // 
            // pictureBoxSymbol
            // 
            this.pictureBoxSymbol.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxSymbol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSymbol.Location = new System.Drawing.Point(894, 195);
            this.pictureBoxSymbol.Name = "pictureBoxSymbol";
            this.pictureBoxSymbol.Size = new System.Drawing.Size(199, 183);
            this.pictureBoxSymbol.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSymbol.TabIndex = 143;
            this.pictureBoxSymbol.TabStop = false;
            // 
            // UpdateParty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources._3d_desktop_technology_wallpaper_backgrounds_for_download1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1362, 742);
            this.Controls.Add(this.buttonSelectSymbol);
            this.Controls.Add(this.pictureBoxSymbol);
            this.Controls.Add(this.comboBoxPID);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.comboBoxNoCandi);
            this.Controls.Add(this.textBoxLeader);
            this.Controls.Add(this.textBoxPname);
            this.Controls.Add(this.labelGender);
            this.Controls.Add(this.labelFname);
            this.Controls.Add(this.labelLName);
            this.Controls.Add(this.labelPID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Name = "UpdateParty";
            this.Text = "UpdateParty";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.UpdateParty_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSymbol)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.ComboBox comboBoxNoCandi;
        private System.Windows.Forms.TextBox textBoxLeader;
        private System.Windows.Forms.TextBox textBoxPname;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelFname;
        private System.Windows.Forms.Label labelLName;
        private System.Windows.Forms.Label labelPID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ComboBox comboBoxPID;
        private System.Windows.Forms.Button buttonSelectSymbol;
        private System.Windows.Forms.PictureBox pictureBoxSymbol;
    }
}
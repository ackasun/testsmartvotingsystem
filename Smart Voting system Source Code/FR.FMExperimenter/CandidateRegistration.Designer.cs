﻿namespace PatternRecognition.FingerprintRecognition.Applications
{
    partial class CandidateRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.buttonRegister = new System.Windows.Forms.Button();
            this.textBoxCContNo = new System.Windows.Forms.TextBox();
            this.labelContactno = new System.Windows.Forms.Label();
            this.textBoxCEmail = new System.Windows.Forms.TextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.textBoxCNIC = new System.Windows.Forms.TextBox();
            this.labelNIC = new System.Windows.Forms.Label();
            this.comboBoxCGender = new System.Windows.Forms.ComboBox();
            this.textBoxCFname = new System.Windows.Forms.TextBox();
            this.labelGender = new System.Windows.Forms.Label();
            this.labelFname = new System.Windows.Forms.Label();
            this.textBoxCID = new System.Windows.Forms.TextBox();
            this.labelVID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPID = new System.Windows.Forms.TextBox();
            this.textBoxPname = new System.Windows.Forms.TextBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxCUname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCandidate = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonSelectSymbol = new System.Windows.Forms.Button();
            this.comboBoxPost = new System.Windows.Forms.ComboBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCandidate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Cyan;
            this.label6.Location = new System.Drawing.Point(559, 690);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(223, 15);
            this.label6.TabIndex = 59;
            this.label6.Text = "ICBT Campus @ All Rights Reservered";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.Transparent;
            this.buttonBack.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBack.ForeColor = System.Drawing.Color.Black;
            this.buttonBack.Location = new System.Drawing.Point(946, 625);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(178, 51);
            this.buttonBack.TabIndex = 58;
            this.buttonBack.Text = "Back";
            this.buttonBack.UseVisualStyleBackColor = false;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // buttonRegister
            // 
            this.buttonRegister.BackColor = System.Drawing.Color.Transparent;
            this.buttonRegister.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegister.ForeColor = System.Drawing.Color.Black;
            this.buttonRegister.Location = new System.Drawing.Point(222, 625);
            this.buttonRegister.Name = "buttonRegister";
            this.buttonRegister.Size = new System.Drawing.Size(178, 51);
            this.buttonRegister.TabIndex = 57;
            this.buttonRegister.Text = "Register";
            this.buttonRegister.UseVisualStyleBackColor = false;
            this.buttonRegister.Click += new System.EventHandler(this.buttonRegister_Click);
            // 
            // textBoxCContNo
            // 
            this.textBoxCContNo.BackColor = System.Drawing.Color.White;
            this.textBoxCContNo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCContNo.Location = new System.Drawing.Point(410, 390);
            this.textBoxCContNo.Name = "textBoxCContNo";
            this.textBoxCContNo.Size = new System.Drawing.Size(177, 26);
            this.textBoxCContNo.TabIndex = 55;
            // 
            // labelContactno
            // 
            this.labelContactno.AutoSize = true;
            this.labelContactno.BackColor = System.Drawing.Color.Transparent;
            this.labelContactno.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelContactno.ForeColor = System.Drawing.Color.Cyan;
            this.labelContactno.Location = new System.Drawing.Point(220, 391);
            this.labelContactno.Name = "labelContactno";
            this.labelContactno.Size = new System.Drawing.Size(104, 22);
            this.labelContactno.TabIndex = 54;
            this.labelContactno.Text = "Contact No";
            this.labelContactno.Click += new System.EventHandler(this.labelContactno_Click);
            // 
            // textBoxCEmail
            // 
            this.textBoxCEmail.BackColor = System.Drawing.Color.White;
            this.textBoxCEmail.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCEmail.Location = new System.Drawing.Point(410, 348);
            this.textBoxCEmail.Name = "textBoxCEmail";
            this.textBoxCEmail.Size = new System.Drawing.Size(231, 26);
            this.textBoxCEmail.TabIndex = 53;
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.BackColor = System.Drawing.Color.Transparent;
            this.labelEmail.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmail.ForeColor = System.Drawing.Color.Cyan;
            this.labelEmail.Location = new System.Drawing.Point(220, 348);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(67, 22);
            this.labelEmail.TabIndex = 52;
            this.labelEmail.Text = "E-Mail";
            this.labelEmail.Click += new System.EventHandler(this.labelEmail_Click);
            // 
            // textBoxCNIC
            // 
            this.textBoxCNIC.BackColor = System.Drawing.Color.White;
            this.textBoxCNIC.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCNIC.Location = new System.Drawing.Point(410, 300);
            this.textBoxCNIC.Name = "textBoxCNIC";
            this.textBoxCNIC.Size = new System.Drawing.Size(177, 26);
            this.textBoxCNIC.TabIndex = 51;
            // 
            // labelNIC
            // 
            this.labelNIC.AutoSize = true;
            this.labelNIC.BackColor = System.Drawing.Color.Transparent;
            this.labelNIC.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNIC.ForeColor = System.Drawing.Color.Cyan;
            this.labelNIC.Location = new System.Drawing.Point(220, 299);
            this.labelNIC.Name = "labelNIC";
            this.labelNIC.Size = new System.Drawing.Size(74, 22);
            this.labelNIC.TabIndex = 50;
            this.labelNIC.Text = "NIC No";
            this.labelNIC.Click += new System.EventHandler(this.labelNIC_Click);
            // 
            // comboBoxCGender
            // 
            this.comboBoxCGender.BackColor = System.Drawing.Color.White;
            this.comboBoxCGender.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCGender.FormattingEnabled = true;
            this.comboBoxCGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.comboBoxCGender.Location = new System.Drawing.Point(410, 250);
            this.comboBoxCGender.Name = "comboBoxCGender";
            this.comboBoxCGender.Size = new System.Drawing.Size(121, 27);
            this.comboBoxCGender.TabIndex = 49;
            this.comboBoxCGender.Text = "Select Gender";
            // 
            // textBoxCFname
            // 
            this.textBoxCFname.BackColor = System.Drawing.Color.White;
            this.textBoxCFname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCFname.Location = new System.Drawing.Point(410, 202);
            this.textBoxCFname.Name = "textBoxCFname";
            this.textBoxCFname.Size = new System.Drawing.Size(231, 26);
            this.textBoxCFname.TabIndex = 47;
            // 
            // labelGender
            // 
            this.labelGender.AutoSize = true;
            this.labelGender.BackColor = System.Drawing.Color.Transparent;
            this.labelGender.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGender.ForeColor = System.Drawing.Color.Cyan;
            this.labelGender.Location = new System.Drawing.Point(220, 250);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(71, 22);
            this.labelGender.TabIndex = 46;
            this.labelGender.Text = "Gender";
            this.labelGender.Click += new System.EventHandler(this.labelGender_Click);
            // 
            // labelFname
            // 
            this.labelFname.AutoSize = true;
            this.labelFname.BackColor = System.Drawing.Color.Transparent;
            this.labelFname.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFname.ForeColor = System.Drawing.Color.Cyan;
            this.labelFname.Location = new System.Drawing.Point(219, 202);
            this.labelFname.Name = "labelFname";
            this.labelFname.Size = new System.Drawing.Size(115, 22);
            this.labelFname.TabIndex = 45;
            this.labelFname.Text = "Name in Full";
            this.labelFname.Click += new System.EventHandler(this.labelFname_Click);
            // 
            // textBoxCID
            // 
            this.textBoxCID.BackColor = System.Drawing.Color.White;
            this.textBoxCID.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCID.Location = new System.Drawing.Point(410, 158);
            this.textBoxCID.Name = "textBoxCID";
            this.textBoxCID.Size = new System.Drawing.Size(100, 26);
            this.textBoxCID.TabIndex = 43;
            // 
            // labelVID
            // 
            this.labelVID.AutoSize = true;
            this.labelVID.BackColor = System.Drawing.Color.Transparent;
            this.labelVID.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVID.ForeColor = System.Drawing.Color.Cyan;
            this.labelVID.Location = new System.Drawing.Point(219, 158);
            this.labelVID.Name = "labelVID";
            this.labelVID.Size = new System.Drawing.Size(120, 22);
            this.labelVID.TabIndex = 42;
            this.labelVID.Text = "Candidate ID";
            this.labelVID.Click += new System.EventHandler(this.labelVID_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Cyan;
            this.label1.Location = new System.Drawing.Point(422, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(501, 55);
            this.label1.TabIndex = 41;
            this.label1.Text = "Candidate Registration\r\n";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Cyan;
            this.label2.Location = new System.Drawing.Point(220, 438);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 22);
            this.label2.TabIndex = 60;
            this.label2.Text = "Party ID";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Cyan;
            this.label3.Location = new System.Drawing.Point(220, 489);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 22);
            this.label3.TabIndex = 61;
            this.label3.Text = "Party Name";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // textBoxPID
            // 
            this.textBoxPID.BackColor = System.Drawing.Color.White;
            this.textBoxPID.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPID.Location = new System.Drawing.Point(410, 437);
            this.textBoxPID.Name = "textBoxPID";
            this.textBoxPID.Size = new System.Drawing.Size(79, 26);
            this.textBoxPID.TabIndex = 62;
            this.textBoxPID.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxPname
            // 
            this.textBoxPname.BackColor = System.Drawing.Color.White;
            this.textBoxPname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPname.Location = new System.Drawing.Point(410, 485);
            this.textBoxPname.Name = "textBoxPname";
            this.textBoxPname.Size = new System.Drawing.Size(177, 26);
            this.textBoxPname.TabIndex = 63;
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.Transparent;
            this.buttonClear.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClear.ForeColor = System.Drawing.Color.Black;
            this.buttonClear.Location = new System.Drawing.Point(583, 625);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(178, 51);
            this.buttonClear.TabIndex = 64;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(956, 158);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(168, 204);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 56;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxCUname
            // 
            this.textBoxCUname.BackColor = System.Drawing.Color.White;
            this.textBoxCUname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCUname.Location = new System.Drawing.Point(410, 540);
            this.textBoxCUname.Name = "textBoxCUname";
            this.textBoxCUname.Size = new System.Drawing.Size(177, 26);
            this.textBoxCUname.TabIndex = 93;
            this.textBoxCUname.TextChanged += new System.EventHandler(this.textBoxCUname_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.label4.Location = new System.Drawing.Point(218, 535);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 22);
            this.label4.TabIndex = 92;
            this.label4.Text = "User Name";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.ICBTkandy;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(273, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1098, 94);
            this.pictureBox2.TabIndex = 101;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.client_img_1;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(277, 94);
            this.pictureBox3.TabIndex = 100;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBoxCandidate
            // 
            this.pictureBoxCandidate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxCandidate.Location = new System.Drawing.Point(956, 391);
            this.pictureBoxCandidate.Name = "pictureBoxCandidate";
            this.pictureBoxCandidate.Size = new System.Drawing.Size(168, 147);
            this.pictureBoxCandidate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCandidate.TabIndex = 102;
            this.pictureBoxCandidate.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Cyan;
            this.label5.Location = new System.Drawing.Point(966, 365);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 19);
            this.label5.TabIndex = 103;
            this.label5.Text = "Sample needed";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.label7.Location = new System.Drawing.Point(220, 582);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 22);
            this.label7.TabIndex = 104;
            this.label7.Text = "Post";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // buttonSelectSymbol
            // 
            this.buttonSelectSymbol.BackColor = System.Drawing.Color.Transparent;
            this.buttonSelectSymbol.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectSymbol.ForeColor = System.Drawing.Color.Black;
            this.buttonSelectSymbol.Location = new System.Drawing.Point(956, 540);
            this.buttonSelectSymbol.Name = "buttonSelectSymbol";
            this.buttonSelectSymbol.Size = new System.Drawing.Size(168, 40);
            this.buttonSelectSymbol.TabIndex = 106;
            this.buttonSelectSymbol.Text = "Select Symbol";
            this.buttonSelectSymbol.UseVisualStyleBackColor = false;
            this.buttonSelectSymbol.Click += new System.EventHandler(this.buttonSelectSymbol_Click);
            // 
            // comboBoxPost
            // 
            this.comboBoxPost.BackColor = System.Drawing.Color.White;
            this.comboBoxPost.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPost.FormattingEnabled = true;
            this.comboBoxPost.Items.AddRange(new object[] {
            "President",
            "Vice-President",
            "Secretary",
            "Vice-Secretary",
            "Treasurer"});
            this.comboBoxPost.Location = new System.Drawing.Point(410, 582);
            this.comboBoxPost.Name = "comboBoxPost";
            this.comboBoxPost.Size = new System.Drawing.Size(177, 27);
            this.comboBoxPost.TabIndex = 107;
            this.comboBoxPost.Text = "Select Post";
            this.comboBoxPost.SelectedIndexChanged += new System.EventHandler(this.comboBoxPost_SelectedIndexChanged);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.back;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(956, 635);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(34, 31);
            this.pictureBox6.TabIndex = 110;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.kripto_clear_b;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(598, 635);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(34, 31);
            this.pictureBox5.TabIndex = 109;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.regist;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(232, 635);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(34, 31);
            this.pictureBox4.TabIndex = 108;
            this.pictureBox4.TabStop = false;
            // 
            // CandidateRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources._3d_desktop_technology_wallpaper_backgrounds_for_download1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1370, 750);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.comboBoxPost);
            this.Controls.Add(this.buttonSelectSymbol);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBoxCandidate);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.textBoxCUname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.textBoxPname);
            this.Controls.Add(this.textBoxPID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.buttonRegister);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBoxCContNo);
            this.Controls.Add(this.labelContactno);
            this.Controls.Add(this.textBoxCEmail);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.textBoxCNIC);
            this.Controls.Add(this.labelNIC);
            this.Controls.Add(this.comboBoxCGender);
            this.Controls.Add(this.textBoxCFname);
            this.Controls.Add(this.labelGender);
            this.Controls.Add(this.labelFname);
            this.Controls.Add(this.textBoxCID);
            this.Controls.Add(this.labelVID);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.Name = "CandidateRegistration";
            this.Text = "CandidateRegistration";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CandidateRegistration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCandidate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Button buttonRegister;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxCContNo;
        private System.Windows.Forms.Label labelContactno;
        private System.Windows.Forms.TextBox textBoxCEmail;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.TextBox textBoxCNIC;
        private System.Windows.Forms.Label labelNIC;
        private System.Windows.Forms.ComboBox comboBoxCGender;
        private System.Windows.Forms.TextBox textBoxCFname;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelFname;
        private System.Windows.Forms.TextBox textBoxCID;
        private System.Windows.Forms.Label labelVID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPID;
        private System.Windows.Forms.TextBox textBoxPname;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.TextBox textBoxCUname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBoxCandidate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonSelectSymbol;
        private System.Windows.Forms.ComboBox comboBoxPost;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}
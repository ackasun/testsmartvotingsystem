﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using BLLayer;
using DBLayer;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class UpdateParty : Form
    {
        public UpdateParty()
        {
            InitializeComponent();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            AdminPanel obj = new AdminPanel();
            this.Hide();
            obj.Show();
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
           
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            //Create windowsDBLayer Registration object
            DBLayer.Party obj = new DBLayer.Party();

            obj.PID1 = comboBoxPID.SelectedItem.ToString();
            obj.Pname1 = textBoxPname.Text.Trim();
            obj.PLeader1 = textBoxLeader.Text.Trim();
            obj.NoOfCandidates1 = comboBoxNoCandi.Text;
           

            obj.UpdateParty(obj);
            //output message if update is successfull
            MessageBox.Show("Party information Updated Successfully !!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            comboBoxPID.Text = String.Empty;
            textBoxPname.Text = String.Empty;
            textBoxLeader.Text = String.Empty;
            comboBoxNoCandi.Text = String.Empty;
         
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            comboBoxPID.Text = String.Empty;
            textBoxPname.Text = String.Empty;
            textBoxLeader.Text = String.Empty;
            comboBoxNoCandi.Text= String.Empty;
           
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxNoCandi_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBoxLeader_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxPname_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelGender_Click(object sender, EventArgs e)
        {

        }

        private void labelFname_Click(object sender, EventArgs e)
        {

        }

        private void labelLName_Click(object sender, EventArgs e)
        {

        }

        private void textBoxPID_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelPID_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            //Create windowsDBLayer Registration object
            DBLayer.Party obj = new DBLayer.Party();

            obj.PID1 = comboBoxPID.SelectedItem.ToString();


            obj.DeleteParty(obj);
            //output message if update is successfull
            MessageBox.Show("Party information Removed Successfully !!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            comboBoxPID.Text = String.Empty;
            textBoxPname.Text = String.Empty;
            textBoxLeader.Text = String.Empty;
            comboBoxNoCandi.Text = String.Empty;
        }

        private void UpdateParty_Load(object sender, EventArgs e)
        {
            try
            {


                MySqlConnection mcon = new MySqlConnection(@"server=localhost;database=icbtelection;username=root;password=;");
                string s = "SELECT * from party";

                mcon.Open();
                MySqlCommand mcd = new MySqlCommand(s, mcon);
                MySqlDataReader mdr = mcd.ExecuteReader();
                while (mdr.Read())
                {

                    comboBoxPID.Items.Add(mdr.GetString("PID"));

                }
                mcon.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void comboBoxPID_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String constring = "server=localhost;database=icbtelection;username=root;password=;";
                String Queary = "SELECT * from party WHERE PID='" + comboBoxPID.Text + "'";
                MySqlConnection conDatabase = new MySqlConnection(constring);
                MySqlCommand cmdDatabase = new MySqlCommand(Queary, conDatabase);
                MySqlDataReader rd;
                try
                {

                    conDatabase.Open();
                    rd = cmdDatabase.ExecuteReader();
                    while (rd.Read())
                    {
                        textBoxPname.Text = (String)rd["Pname"];
                        textBoxLeader.Text = (String)rd["PLeader"];
                        comboBoxNoCandi.Text = (String)rd["NoOfCandidates"];
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        private void buttonSelectSymbol_Click(object sender, EventArgs e)
        {

        }
        
    }
}

﻿namespace PatternRecognition.FingerprintRecognition.Applications
{
    partial class UpdateAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRemove = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonClear = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelContactno = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.labelNIC = new System.Windows.Forms.Label();
            this.labelGender = new System.Windows.Forms.Label();
            this.labelFname = new System.Windows.Forms.Label();
            this.labelLName = new System.Windows.Forms.Label();
            this.labelVID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAUname = new System.Windows.Forms.TextBox();
            this.textBoxAContNo = new System.Windows.Forms.TextBox();
            this.textBoxAEmail = new System.Windows.Forms.TextBox();
            this.textBoxANIC = new System.Windows.Forms.TextBox();
            this.comboBoxAGender = new System.Windows.Forms.ComboBox();
            this.textBoxALname = new System.Windows.Forms.TextBox();
            this.textBoxAFname = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBoxAID = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonRemove
            // 
            this.buttonRemove.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRemove.Location = new System.Drawing.Point(469, 581);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(178, 51);
            this.buttonRemove.TabIndex = 121;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Cyan;
            this.label4.Location = new System.Drawing.Point(255, 516);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 22);
            this.label4.TabIndex = 117;
            this.label4.Text = "User Name";
            // 
            // buttonClear
            // 
            this.buttonClear.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClear.Location = new System.Drawing.Point(694, 581);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(178, 51);
            this.buttonClear.TabIndex = 116;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Cyan;
            this.label6.Location = new System.Drawing.Point(558, 666);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(223, 15);
            this.label6.TabIndex = 114;
            this.label6.Text = "ICBT Campus @ All Rights Reservered";
            // 
            // buttonBack
            // 
            this.buttonBack.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBack.Location = new System.Drawing.Point(919, 581);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(178, 51);
            this.buttonBack.TabIndex = 113;
            this.buttonBack.Text = "Back";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdate.Location = new System.Drawing.Point(259, 581);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(178, 51);
            this.buttonUpdate.TabIndex = 112;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelContactno
            // 
            this.labelContactno.AutoSize = true;
            this.labelContactno.BackColor = System.Drawing.Color.Transparent;
            this.labelContactno.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelContactno.ForeColor = System.Drawing.Color.Cyan;
            this.labelContactno.Location = new System.Drawing.Point(252, 470);
            this.labelContactno.Name = "labelContactno";
            this.labelContactno.Size = new System.Drawing.Size(104, 22);
            this.labelContactno.TabIndex = 110;
            this.labelContactno.Text = "Contact No";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.BackColor = System.Drawing.Color.Transparent;
            this.labelEmail.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmail.ForeColor = System.Drawing.Color.Cyan;
            this.labelEmail.Location = new System.Drawing.Point(252, 427);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(67, 22);
            this.labelEmail.TabIndex = 108;
            this.labelEmail.Text = "E-Mail";
            // 
            // labelNIC
            // 
            this.labelNIC.AutoSize = true;
            this.labelNIC.BackColor = System.Drawing.Color.Transparent;
            this.labelNIC.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNIC.ForeColor = System.Drawing.Color.Cyan;
            this.labelNIC.Location = new System.Drawing.Point(252, 378);
            this.labelNIC.Name = "labelNIC";
            this.labelNIC.Size = new System.Drawing.Size(74, 22);
            this.labelNIC.TabIndex = 106;
            this.labelNIC.Text = "NIC No";
            // 
            // labelGender
            // 
            this.labelGender.AutoSize = true;
            this.labelGender.BackColor = System.Drawing.Color.Transparent;
            this.labelGender.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGender.ForeColor = System.Drawing.Color.Cyan;
            this.labelGender.Location = new System.Drawing.Point(252, 329);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(71, 22);
            this.labelGender.TabIndex = 102;
            this.labelGender.Text = "Gender";
            // 
            // labelFname
            // 
            this.labelFname.AutoSize = true;
            this.labelFname.BackColor = System.Drawing.Color.Transparent;
            this.labelFname.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFname.ForeColor = System.Drawing.Color.Cyan;
            this.labelFname.Location = new System.Drawing.Point(251, 242);
            this.labelFname.Name = "labelFname";
            this.labelFname.Size = new System.Drawing.Size(102, 22);
            this.labelFname.TabIndex = 101;
            this.labelFname.Text = "First Name";
            // 
            // labelLName
            // 
            this.labelLName.AutoSize = true;
            this.labelLName.BackColor = System.Drawing.Color.Transparent;
            this.labelLName.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLName.ForeColor = System.Drawing.Color.Cyan;
            this.labelLName.Location = new System.Drawing.Point(252, 283);
            this.labelLName.Name = "labelLName";
            this.labelLName.Size = new System.Drawing.Size(99, 22);
            this.labelLName.TabIndex = 100;
            this.labelLName.Text = "Last Name";
            // 
            // labelVID
            // 
            this.labelVID.AutoSize = true;
            this.labelVID.BackColor = System.Drawing.Color.Transparent;
            this.labelVID.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVID.ForeColor = System.Drawing.Color.Cyan;
            this.labelVID.Location = new System.Drawing.Point(251, 198);
            this.labelVID.Name = "labelVID";
            this.labelVID.Size = new System.Drawing.Size(89, 22);
            this.labelVID.TabIndex = 98;
            this.labelVID.Text = "Admin ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Cyan;
            this.label1.Location = new System.Drawing.Point(459, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(504, 55);
            this.label1.TabIndex = 97;
            this.label1.Text = "Update/Remove Admin";
            // 
            // textBoxAUname
            // 
            this.textBoxAUname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAUname.Location = new System.Drawing.Point(540, 516);
            this.textBoxAUname.Name = "textBoxAUname";
            this.textBoxAUname.Size = new System.Drawing.Size(177, 26);
            this.textBoxAUname.TabIndex = 129;
            // 
            // textBoxAContNo
            // 
            this.textBoxAContNo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAContNo.Location = new System.Drawing.Point(540, 465);
            this.textBoxAContNo.Name = "textBoxAContNo";
            this.textBoxAContNo.Size = new System.Drawing.Size(177, 26);
            this.textBoxAContNo.TabIndex = 128;
            // 
            // textBoxAEmail
            // 
            this.textBoxAEmail.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAEmail.Location = new System.Drawing.Point(540, 423);
            this.textBoxAEmail.Name = "textBoxAEmail";
            this.textBoxAEmail.Size = new System.Drawing.Size(231, 26);
            this.textBoxAEmail.TabIndex = 127;
            // 
            // textBoxANIC
            // 
            this.textBoxANIC.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxANIC.Location = new System.Drawing.Point(540, 375);
            this.textBoxANIC.Name = "textBoxANIC";
            this.textBoxANIC.Size = new System.Drawing.Size(177, 26);
            this.textBoxANIC.TabIndex = 126;
            // 
            // comboBoxAGender
            // 
            this.comboBoxAGender.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxAGender.FormattingEnabled = true;
            this.comboBoxAGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.comboBoxAGender.Location = new System.Drawing.Point(540, 325);
            this.comboBoxAGender.Name = "comboBoxAGender";
            this.comboBoxAGender.Size = new System.Drawing.Size(147, 27);
            this.comboBoxAGender.TabIndex = 125;
            // 
            // textBoxALname
            // 
            this.textBoxALname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxALname.Location = new System.Drawing.Point(540, 279);
            this.textBoxALname.Name = "textBoxALname";
            this.textBoxALname.Size = new System.Drawing.Size(231, 26);
            this.textBoxALname.TabIndex = 124;
            // 
            // textBoxAFname
            // 
            this.textBoxAFname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAFname.Location = new System.Drawing.Point(540, 238);
            this.textBoxAFname.Name = "textBoxAFname";
            this.textBoxAFname.Size = new System.Drawing.Size(231, 26);
            this.textBoxAFname.TabIndex = 123;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.ICBTkandy;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(273, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1098, 94);
            this.pictureBox2.TabIndex = 131;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.client_img_1;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(-1, 1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(277, 94);
            this.pictureBox3.TabIndex = 130;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.refresh_512;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(273, 591);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(34, 31);
            this.pictureBox4.TabIndex = 132;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.back;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(929, 591);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(34, 31);
            this.pictureBox6.TabIndex = 134;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.kripto_clear_b;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(706, 591);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(34, 31);
            this.pictureBox5.TabIndex = 133;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.editor_trash_delete_recycle_bin__512;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(481, 591);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(34, 31);
            this.pictureBox1.TabIndex = 135;
            this.pictureBox1.TabStop = false;
            // 
            // comboBoxAID
            // 
            this.comboBoxAID.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxAID.FormattingEnabled = true;
            this.comboBoxAID.Items.AddRange(new object[] {
            "Select Admin ID"});
            this.comboBoxAID.Location = new System.Drawing.Point(540, 193);
            this.comboBoxAID.Name = "comboBoxAID";
            this.comboBoxAID.Size = new System.Drawing.Size(140, 27);
            this.comboBoxAID.TabIndex = 136;
            this.comboBoxAID.Text = "Select Admin ID";
            this.comboBoxAID.SelectedIndexChanged += new System.EventHandler(this.comboBoxAID_SelectedIndexChanged);
            // 
            // UpdateAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources._3d_desktop_technology_wallpaper_backgrounds_for_download1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1362, 742);
            this.Controls.Add(this.comboBoxAID);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.textBoxAUname);
            this.Controls.Add(this.textBoxAContNo);
            this.Controls.Add(this.textBoxAEmail);
            this.Controls.Add(this.textBoxANIC);
            this.Controls.Add(this.comboBoxAGender);
            this.Controls.Add(this.textBoxALname);
            this.Controls.Add(this.textBoxAFname);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.labelContactno);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.labelNIC);
            this.Controls.Add(this.labelGender);
            this.Controls.Add(this.labelFname);
            this.Controls.Add(this.labelLName);
            this.Controls.Add(this.labelVID);
            this.Controls.Add(this.label1);
            this.Name = "UpdateAdmin";
            this.Text = "UpdateAdmin";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.UpdateAdmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelContactno;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label labelNIC;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelFname;
        private System.Windows.Forms.Label labelLName;
        private System.Windows.Forms.Label labelVID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAUname;
        private System.Windows.Forms.TextBox textBoxAContNo;
        private System.Windows.Forms.TextBox textBoxAEmail;
        private System.Windows.Forms.TextBox textBoxANIC;
        private System.Windows.Forms.ComboBox comboBoxAGender;
        private System.Windows.Forms.TextBox textBoxALname;
        private System.Windows.Forms.TextBox textBoxAFname;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBoxAID;
    }
}
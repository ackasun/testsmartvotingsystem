﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using DBLayer;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class VotePresident : Form
    {
        public VotePresident()
        {
            InitializeComponent();
        }

        private void comboBoxPname_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void VotingPanel_Load(object sender, EventArgs e)
        {
            
               try
                {


                    MySqlConnection mcon = new MySqlConnection(@"server=localhost;database=icbtelection;username=root;password=;");
                    string s = "SELECT * from candidate WHERE Post='President'";

                    mcon.Open();
                    MySqlCommand mcd = new MySqlCommand(s, mcon);
                    MySqlDataReader mdr = mcd.ExecuteReader();
                    while (mdr.Read())
                    {

                        comboBoxCandidateName.Items.Add(mdr.GetString("CFname"));

                    }
                    mcon.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            
         
          
        }

       
        private void buttonCastVote_Click(object sender, EventArgs e)
        {
          
        }

        private void comboBoxCandidateName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String constring = "server=localhost;database=icbtelection;username=root;password=;";
                String Queary = "SELECT * from candidate WHERE CFname='" + comboBoxCandidateName.Text + "'";
                MySqlConnection conDatabase = new MySqlConnection(constring);
                MySqlCommand cmdDatabase = new MySqlCommand(Queary, conDatabase);
                MySqlDataReader myReader;
                try
                {

                    conDatabase.Open();
                    myReader = cmdDatabase.ExecuteReader();
                    while (myReader.Read())
                    {
                        textBoxPname.Text = myReader.GetString("Pname");
                        textBoxGender.Text = myReader.GetString("CGender");
                        byte[] img = (byte[])(myReader["Image"]);
                        MemoryStream mstream = new MemoryStream(img);
                        pictureBoxImage.Image = System.Drawing.Image.FromStream(mstream);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            if (comboBoxCandidateName.Text == "")
            {
                MessageBox.Show("Select a Candidate to cast the vote !!", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DBLayer.Vote obj = new DBLayer.Vote();

                obj.Name1 = comboBoxCandidateName.SelectedItem.ToString();
                obj.UpdateVotes(obj);
                //output message if update is successfull
                MessageBox.Show("Successfully Voted", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ButtonVote.Enabled = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            comboBoxCandidateName.Text = String.Empty;
            textBoxPname.Text = String.Empty;
            textBoxGender.Text = String.Empty;
            pictureBoxImage.Image = null;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void updateRegistrationInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
                    }

        private void candidateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VoteVicePrasident obj = new VoteVicePrasident();
            this.Hide();
            obj.Show();
        }

        private void administratorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VoteViceSecretary obj = new VoteViceSecretary();
            this.Hide();
            obj.Show();
        }

        private void voterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VotePresident obj = new VotePresident();
            this.Hide();
            obj.Show();
        }

        private void logOutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Welcome obj = new Welcome();
            this.Hide();
            obj.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBoxPname_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBoxImage_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void partyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VoteSecretary obj = new VoteSecretary();
            this.Hide();
            obj.Show();
        }

        private void votesCalculationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VoteTreasurer obj = new VoteTreasurer();
            this.Hide();
            obj.Show();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VoteViceTreasurer obj = new VoteViceTreasurer();
            this.Hide();
            obj.Show();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBoxGender_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }
    }
}

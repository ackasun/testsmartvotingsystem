﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class VotingGuide : Form
    {
        public VotingGuide()
        {
            InitializeComponent();
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            VotePresident obj = new VotePresident();
            this.Hide();
            obj.Show();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            Welcome obj = new Welcome();
            this.Hide();
            obj.Show();
        }
    }
}

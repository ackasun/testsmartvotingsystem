﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using DBLayer;
namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class UpdateCandidate : Form
    {
        public UpdateCandidate()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void textBoxCUname_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            AdminPanel obj = new AdminPanel();
            this.Hide();
            obj.Show();
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
           
        }

        private void buttonUpdateCan_Click(object sender, EventArgs e)
        {
           
            //Create windowsDBLayer Registration object
            DBLayer.Candidate obj = new DBLayer.Candidate();

            obj.CID1 = comboBoxCID.SelectedItem.ToString();
            obj.CFname1 = textBoxCFname.Text.Trim();
            obj.CGender1 = comboBoxCGender.SelectedItem.ToString();
            obj.CNIC1 =  textBoxCNIC.Text.Trim();
            obj.CEmail1 = textBoxCEmail.Text.Trim();
            obj.CContNo1 = textBoxCContNo.Text.Trim();
            obj.Post1 = comboBoxPost.SelectedItem.ToString();
            obj.PID1 =   textBoxPID.Text.Trim();
            obj.Pname1 = textBoxPname.Text.Trim();
            obj.Uname1 = textBoxCUname.Text.Trim();
            

            obj.UpdateCandidate(obj);
            //output message if update is successfull
            MessageBox.Show("Candidate information Updated Successfully !!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            comboBoxCID.Text = String.Empty;
            textBoxCFname.Text = String.Empty;
            comboBoxCGender.Text = String.Empty;
            textBoxCNIC.Text = String.Empty;
            textBoxCEmail.Text = String.Empty;
            textBoxCContNo.Text = String.Empty;
            textBoxPID.Text = String.Empty;
            textBoxPname.Text = String.Empty;
            textBoxCUname.Text = String.Empty;
            comboBoxPost.Text = String.Empty;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            comboBoxCID.Text = String.Empty;
            textBoxCFname.Text = String.Empty;
            comboBoxCGender.Text = String.Empty;
            textBoxCNIC.Text = String.Empty;
            textBoxCEmail.Text = String.Empty;
            textBoxCContNo.Text = String.Empty;
            textBoxPID.Text = String.Empty;
            textBoxPname.Text = String.Empty;
            textBoxCUname.Text = String.Empty;
            comboBoxPost.Text = String.Empty;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBoxPname_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxPID_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBoxCContNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelContactno_Click(object sender, EventArgs e)
        {

        }

        private void textBoxCEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelEmail_Click(object sender, EventArgs e)
        {

        }

        private void textBoxCNIC_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelNIC_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxCGender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBoxCLname_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxCFname_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelGender_Click(object sender, EventArgs e)
        {

        }

        private void labelFname_Click(object sender, EventArgs e)
        {

        }

        private void labelLName_Click(object sender, EventArgs e)
        {

        }

        private void textBoxCID_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelVID_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Create windowsDBLayer Registration object
            DBLayer.Candidate obj = new DBLayer.Candidate();

            obj.CID1 = comboBoxCID.SelectedItem.ToString();


            obj.DeleteCandidate(obj);
            //output message if update is successfull
            MessageBox.Show("Candidate information Removed Successfully !!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            comboBoxCID.Text = String.Empty;
            textBoxCFname.Text = String.Empty;
            comboBoxCGender.Text = String.Empty;
            textBoxCNIC.Text = String.Empty;
            textBoxCEmail.Text = String.Empty;
            textBoxCContNo.Text = String.Empty;
            textBoxPID.Text = String.Empty;
            textBoxPname.Text = String.Empty;
            textBoxCUname.Text = String.Empty;
            comboBoxPost.Text = String.Empty;
        }

        private void UpdateCandidate_Load(object sender, EventArgs e)
        {
            try
            {


                MySqlConnection mcon = new MySqlConnection(@"server=localhost;database=icbtelection;username=root;password=;");
                string s = "SELECT * from candidate";

                mcon.Open();
                MySqlCommand mcd = new MySqlCommand(s, mcon);
                MySqlDataReader mdr = mcd.ExecuteReader();
                while (mdr.Read())
                {

                    comboBoxCID.Items.Add(mdr.GetString("CID"));

                }
                mcon.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxCID_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String constring = "server=localhost;database=icbtelection;username=root;password=;";
                String Queary = "SELECT * from candidate WHERE CID='" + comboBoxCID.Text + "'";
                MySqlConnection conDatabase = new MySqlConnection(constring);
                MySqlCommand cmdDatabase = new MySqlCommand(Queary, conDatabase);
                MySqlDataReader rd;
                try
                {

                    conDatabase.Open();
                    rd = cmdDatabase.ExecuteReader();
                    while (rd.Read())
                    {
                        textBoxCFname.Text = (String)rd["CFname"];
                        comboBoxCGender.Text = (String)rd["CGender"];
                        textBoxCNIC.Text = (String)rd["CNIC"];
                        textBoxCEmail.Text = (String)rd["CEmail"];
                        textBoxCContNo.Text = (String)rd["CContNo"];
                        textBoxPID.Text = (String)rd["PID"];
                        textBoxPname.Text = (String)rd["Pname"];
                        textBoxCUname.Text = (String)rd["Username"];
                        comboBoxPost.Text = (String)rd["Post"];
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void buttonSelectSymbol_Click(object sender, EventArgs e)
        {

        }
    }
}

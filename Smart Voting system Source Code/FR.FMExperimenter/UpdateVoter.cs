﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;



namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class UpdateVoter : Form
    {
        public UpdateVoter()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            AdminPanel obj = new AdminPanel();
            this.Hide();
            obj.Show();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            //Create windowsDBLayer Registration object
            DBLayer.Voter obj = new DBLayer.Voter();
            obj.VID1 = comboBoxVID.Text.Trim();
            obj.Fname1 = textBoxFname.Text.Trim();
            obj.Lname1= textBoxLname.Text.Trim();
            obj.Gender1 = comboBoxGender.Text.Trim();
            obj.NIC1 = textBoxNIC.Text.Trim();
            obj.Email1 = textBoxEmail.Text.Trim();
            obj.ContNo1 = textBoxContNo.Text.Trim();
            obj.Uname1 = textBoxVUname.Text.Trim();

            obj.UpdateVoter(obj);
            //output message if update is successfull
            MessageBox.Show("Voter information Updated Successfully !!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            comboBoxVID.Text = String.Empty;
            textBoxFname.Text = String.Empty;
            textBoxLname.Text = String.Empty;
            comboBoxGender.Text = String.Empty;
            textBoxNIC.Text = String.Empty;
            textBoxEmail.Text = String.Empty;
            textBoxContNo.Text = String.Empty;
            textBoxVUname.Text = String.Empty;
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
        
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            comboBoxVID.Text = String.Empty;
            textBoxFname.Text = String.Empty;
            textBoxLname.Text = String.Empty;
            comboBoxGender.Text = String.Empty;
            textBoxNIC.Text = String.Empty;
            textBoxEmail.Text = String.Empty;
            textBoxContNo.Text = String.Empty;
            textBoxVUname.Text = String.Empty;
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            //Create windowsDBLayer Registration object
            DBLayer.Voter obj = new DBLayer.Voter();

            obj.VID1 = comboBoxVID.SelectedItem.ToString();
           

            obj.DeleteVoter(obj);
            //output message if update is successfull
            MessageBox.Show("Voter information Removed Successfully !!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            comboBoxVID.Text = String.Empty;
            textBoxFname.Text = String.Empty;
            textBoxLname.Text = String.Empty;
            comboBoxGender.Text = String.Empty;
            textBoxNIC.Text = String.Empty;
            textBoxEmail.Text = String.Empty;
            textBoxContNo.Text = String.Empty;
            textBoxVUname.Text = String.Empty;
        }

        private void UpdateVoter_Load(object sender, EventArgs e)
        {
            try
            {


                MySqlConnection mcon = new MySqlConnection(@"server=localhost;database=icbtelection;username=root;password=;");
                string s = "SELECT * from voter";

                mcon.Open();
                MySqlCommand mcd = new MySqlCommand(s, mcon);
                MySqlDataReader mdr = mcd.ExecuteReader();
                while (mdr.Read())
                {

                    comboBoxVID.Items.Add(mdr.GetString("VID"));

                }
                mcon.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBoxVID_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String constring = "server=localhost;database=icbtelection;username=root;password=;";
                String Queary = "SELECT * from voter WHERE VID='" + comboBoxVID.Text + "'";
                MySqlConnection conDatabase = new MySqlConnection(constring);
                MySqlCommand cmdDatabase = new MySqlCommand(Queary, conDatabase);
                MySqlDataReader rd;
                try
                {

                    conDatabase.Open();
                    rd = cmdDatabase.ExecuteReader();
                    while (rd.Read())
                    {
                        textBoxFname.Text = (String)rd["Fname"];
                        textBoxLname.Text = (String)rd["Lname"];
                        comboBoxGender.Text = (String)rd["Gender"];
                        textBoxNIC.Text = (String)rd["NIC"];
                        textBoxEmail.Text = (String)rd["Email"];
                        textBoxContNo.Text = (String)rd["ContNo"];
                        textBoxVUname.Text = (String)rd["Username"];
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
    }
}

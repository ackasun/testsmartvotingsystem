﻿namespace PatternRecognition.FingerprintRecognition.Applications
{
    partial class UpdateCandidate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonClear = new System.Windows.Forms.Button();
            this.textBoxPname = new System.Windows.Forms.TextBox();
            this.textBoxPID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.buttonUpdateCan = new System.Windows.Forms.Button();
            this.textBoxCContNo = new System.Windows.Forms.TextBox();
            this.labelContactno = new System.Windows.Forms.Label();
            this.textBoxCEmail = new System.Windows.Forms.TextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.textBoxCNIC = new System.Windows.Forms.TextBox();
            this.labelNIC = new System.Windows.Forms.Label();
            this.comboBoxCGender = new System.Windows.Forms.ComboBox();
            this.textBoxCFname = new System.Windows.Forms.TextBox();
            this.labelGender = new System.Windows.Forms.Label();
            this.labelFname = new System.Windows.Forms.Label();
            this.labelVID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxCUname = new System.Windows.Forms.TextBox();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxPost = new System.Windows.Forms.ComboBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.comboBoxCID = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonClear
            // 
            this.buttonClear.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClear.ForeColor = System.Drawing.Color.Black;
            this.buttonClear.Location = new System.Drawing.Point(699, 646);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(178, 51);
            this.buttonClear.TabIndex = 88;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // textBoxPname
            // 
            this.textBoxPname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPname.ForeColor = System.Drawing.Color.Black;
            this.textBoxPname.Location = new System.Drawing.Point(589, 502);
            this.textBoxPname.Name = "textBoxPname";
            this.textBoxPname.Size = new System.Drawing.Size(177, 26);
            this.textBoxPname.TabIndex = 87;
            this.textBoxPname.TextChanged += new System.EventHandler(this.textBoxPname_TextChanged);
            // 
            // textBoxPID
            // 
            this.textBoxPID.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPID.ForeColor = System.Drawing.Color.Black;
            this.textBoxPID.Location = new System.Drawing.Point(589, 463);
            this.textBoxPID.Name = "textBoxPID";
            this.textBoxPID.Size = new System.Drawing.Size(177, 26);
            this.textBoxPID.TabIndex = 86;
            this.textBoxPID.TextChanged += new System.EventHandler(this.textBoxPID_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Cyan;
            this.label3.Location = new System.Drawing.Point(239, 505);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 22);
            this.label3.TabIndex = 85;
            this.label3.Text = "Party Name";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Cyan;
            this.label2.Location = new System.Drawing.Point(238, 468);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 22);
            this.label2.TabIndex = 84;
            this.label2.Text = "Party ID";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Cyan;
            this.label6.Location = new System.Drawing.Point(556, 718);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(223, 15);
            this.label6.TabIndex = 83;
            this.label6.Text = "ICBT Campus @ All Rights Reservered";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBack.ForeColor = System.Drawing.Color.Black;
            this.buttonBack.Location = new System.Drawing.Point(931, 646);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(178, 51);
            this.buttonBack.TabIndex = 82;
            this.buttonBack.Text = "Back";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // buttonUpdateCan
            // 
            this.buttonUpdateCan.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateCan.ForeColor = System.Drawing.Color.Black;
            this.buttonUpdateCan.Location = new System.Drawing.Point(242, 646);
            this.buttonUpdateCan.Name = "buttonUpdateCan";
            this.buttonUpdateCan.Size = new System.Drawing.Size(178, 51);
            this.buttonUpdateCan.TabIndex = 81;
            this.buttonUpdateCan.Text = "Update";
            this.buttonUpdateCan.UseVisualStyleBackColor = true;
            this.buttonUpdateCan.Click += new System.EventHandler(this.buttonUpdateCan_Click);
            // 
            // textBoxCContNo
            // 
            this.textBoxCContNo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCContNo.ForeColor = System.Drawing.Color.Black;
            this.textBoxCContNo.Location = new System.Drawing.Point(589, 417);
            this.textBoxCContNo.Name = "textBoxCContNo";
            this.textBoxCContNo.Size = new System.Drawing.Size(177, 26);
            this.textBoxCContNo.TabIndex = 79;
            this.textBoxCContNo.TextChanged += new System.EventHandler(this.textBoxCContNo_TextChanged);
            // 
            // labelContactno
            // 
            this.labelContactno.AutoSize = true;
            this.labelContactno.BackColor = System.Drawing.Color.Transparent;
            this.labelContactno.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelContactno.ForeColor = System.Drawing.Color.Cyan;
            this.labelContactno.Location = new System.Drawing.Point(238, 422);
            this.labelContactno.Name = "labelContactno";
            this.labelContactno.Size = new System.Drawing.Size(104, 22);
            this.labelContactno.TabIndex = 78;
            this.labelContactno.Text = "Contact No";
            this.labelContactno.Click += new System.EventHandler(this.labelContactno_Click);
            // 
            // textBoxCEmail
            // 
            this.textBoxCEmail.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCEmail.ForeColor = System.Drawing.Color.Black;
            this.textBoxCEmail.Location = new System.Drawing.Point(589, 375);
            this.textBoxCEmail.Name = "textBoxCEmail";
            this.textBoxCEmail.Size = new System.Drawing.Size(231, 26);
            this.textBoxCEmail.TabIndex = 77;
            this.textBoxCEmail.TextChanged += new System.EventHandler(this.textBoxCEmail_TextChanged);
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.BackColor = System.Drawing.Color.Transparent;
            this.labelEmail.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmail.ForeColor = System.Drawing.Color.Cyan;
            this.labelEmail.Location = new System.Drawing.Point(238, 379);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(67, 22);
            this.labelEmail.TabIndex = 76;
            this.labelEmail.Text = "E-Mail";
            this.labelEmail.Click += new System.EventHandler(this.labelEmail_Click);
            // 
            // textBoxCNIC
            // 
            this.textBoxCNIC.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCNIC.ForeColor = System.Drawing.Color.Black;
            this.textBoxCNIC.Location = new System.Drawing.Point(589, 327);
            this.textBoxCNIC.Name = "textBoxCNIC";
            this.textBoxCNIC.Size = new System.Drawing.Size(177, 26);
            this.textBoxCNIC.TabIndex = 75;
            this.textBoxCNIC.TextChanged += new System.EventHandler(this.textBoxCNIC_TextChanged);
            // 
            // labelNIC
            // 
            this.labelNIC.AutoSize = true;
            this.labelNIC.BackColor = System.Drawing.Color.Transparent;
            this.labelNIC.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNIC.ForeColor = System.Drawing.Color.Cyan;
            this.labelNIC.Location = new System.Drawing.Point(238, 330);
            this.labelNIC.Name = "labelNIC";
            this.labelNIC.Size = new System.Drawing.Size(74, 22);
            this.labelNIC.TabIndex = 74;
            this.labelNIC.Text = "NIC No";
            this.labelNIC.Click += new System.EventHandler(this.labelNIC_Click);
            // 
            // comboBoxCGender
            // 
            this.comboBoxCGender.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCGender.ForeColor = System.Drawing.Color.Black;
            this.comboBoxCGender.FormattingEnabled = true;
            this.comboBoxCGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.comboBoxCGender.Location = new System.Drawing.Point(589, 277);
            this.comboBoxCGender.Name = "comboBoxCGender";
            this.comboBoxCGender.Size = new System.Drawing.Size(159, 27);
            this.comboBoxCGender.TabIndex = 73;
            this.comboBoxCGender.Text = "Select Gender";
            this.comboBoxCGender.SelectedIndexChanged += new System.EventHandler(this.comboBoxCGender_SelectedIndexChanged);
            // 
            // textBoxCFname
            // 
            this.textBoxCFname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCFname.ForeColor = System.Drawing.Color.Black;
            this.textBoxCFname.Location = new System.Drawing.Point(588, 231);
            this.textBoxCFname.Name = "textBoxCFname";
            this.textBoxCFname.Size = new System.Drawing.Size(231, 26);
            this.textBoxCFname.TabIndex = 71;
            this.textBoxCFname.TextChanged += new System.EventHandler(this.textBoxCFname_TextChanged);
            // 
            // labelGender
            // 
            this.labelGender.AutoSize = true;
            this.labelGender.BackColor = System.Drawing.Color.Transparent;
            this.labelGender.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGender.ForeColor = System.Drawing.Color.Cyan;
            this.labelGender.Location = new System.Drawing.Point(238, 281);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(71, 22);
            this.labelGender.TabIndex = 70;
            this.labelGender.Text = "Gender";
            this.labelGender.Click += new System.EventHandler(this.labelGender_Click);
            // 
            // labelFname
            // 
            this.labelFname.AutoSize = true;
            this.labelFname.BackColor = System.Drawing.Color.Transparent;
            this.labelFname.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFname.ForeColor = System.Drawing.Color.Cyan;
            this.labelFname.Location = new System.Drawing.Point(236, 235);
            this.labelFname.Name = "labelFname";
            this.labelFname.Size = new System.Drawing.Size(115, 22);
            this.labelFname.TabIndex = 69;
            this.labelFname.Text = "Name in Full";
            this.labelFname.Click += new System.EventHandler(this.labelFname_Click);
            // 
            // labelVID
            // 
            this.labelVID.AutoSize = true;
            this.labelVID.BackColor = System.Drawing.Color.Transparent;
            this.labelVID.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVID.ForeColor = System.Drawing.Color.Cyan;
            this.labelVID.Location = new System.Drawing.Point(236, 191);
            this.labelVID.Name = "labelVID";
            this.labelVID.Size = new System.Drawing.Size(120, 22);
            this.labelVID.TabIndex = 66;
            this.labelVID.Text = "Candidate ID";
            this.labelVID.Click += new System.EventHandler(this.labelVID_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Cyan;
            this.label1.Location = new System.Drawing.Point(408, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(578, 55);
            this.label1.TabIndex = 65;
            this.label1.Text = "Update/Remove Candidate";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Cyan;
            this.label4.Location = new System.Drawing.Point(239, 551);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 22);
            this.label4.TabIndex = 90;
            this.label4.Text = "User Name";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // textBoxCUname
            // 
            this.textBoxCUname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCUname.ForeColor = System.Drawing.Color.Black;
            this.textBoxCUname.Location = new System.Drawing.Point(589, 547);
            this.textBoxCUname.Name = "textBoxCUname";
            this.textBoxCUname.Size = new System.Drawing.Size(177, 26);
            this.textBoxCUname.TabIndex = 91;
            this.textBoxCUname.TextChanged += new System.EventHandler(this.textBoxCUname_TextChanged);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRemove.ForeColor = System.Drawing.Color.Black;
            this.buttonRemove.Location = new System.Drawing.Point(467, 646);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(178, 51);
            this.buttonRemove.TabIndex = 98;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.ICBTkandy;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(272, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1098, 94);
            this.pictureBox2.TabIndex = 107;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.client_img_1;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(-2, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(277, 94);
            this.pictureBox3.TabIndex = 106;
            this.pictureBox3.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Cyan;
            this.label5.Location = new System.Drawing.Point(239, 599);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 22);
            this.label5.TabIndex = 108;
            this.label5.Text = "Post";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // comboBoxPost
            // 
            this.comboBoxPost.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPost.ForeColor = System.Drawing.Color.Black;
            this.comboBoxPost.FormattingEnabled = true;
            this.comboBoxPost.Items.AddRange(new object[] {
            "President",
            "Vice-President",
            "Secretary",
            "Vice-Secretary",
            "Treasurer"});
            this.comboBoxPost.Location = new System.Drawing.Point(588, 594);
            this.comboBoxPost.Name = "comboBoxPost";
            this.comboBoxPost.Size = new System.Drawing.Size(178, 27);
            this.comboBoxPost.TabIndex = 109;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.back;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(940, 655);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(34, 31);
            this.pictureBox6.TabIndex = 112;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.kripto_clear_b;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(714, 655);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(34, 31);
            this.pictureBox5.TabIndex = 111;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.editor_trash_delete_recycle_bin__512;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(478, 655);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(34, 31);
            this.pictureBox1.TabIndex = 137;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources.refresh_512;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(251, 655);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(34, 31);
            this.pictureBox4.TabIndex = 136;
            this.pictureBox4.TabStop = false;
            // 
            // comboBoxCID
            // 
            this.comboBoxCID.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCID.ForeColor = System.Drawing.Color.Black;
            this.comboBoxCID.FormattingEnabled = true;
            this.comboBoxCID.Items.AddRange(new object[] {
            "Select Candidate ID"});
            this.comboBoxCID.Location = new System.Drawing.Point(589, 191);
            this.comboBoxCID.Name = "comboBoxCID";
            this.comboBoxCID.Size = new System.Drawing.Size(159, 27);
            this.comboBoxCID.TabIndex = 138;
            this.comboBoxCID.Text = "Select Candidate ID";
            this.comboBoxCID.SelectedIndexChanged += new System.EventHandler(this.comboBoxCID_SelectedIndexChanged);
            // 
            // UpdateCandidate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PatternRecognition.FingerprintRecognition.Applications.Properties.Resources._3d_desktop_technology_wallpaper_backgrounds_for_download1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1362, 742);
            this.Controls.Add(this.comboBoxCID);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.comboBoxPost);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.textBoxCUname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.textBoxPname);
            this.Controls.Add(this.textBoxPID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.buttonUpdateCan);
            this.Controls.Add(this.textBoxCContNo);
            this.Controls.Add(this.labelContactno);
            this.Controls.Add(this.textBoxCEmail);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.textBoxCNIC);
            this.Controls.Add(this.labelNIC);
            this.Controls.Add(this.comboBoxCGender);
            this.Controls.Add(this.textBoxCFname);
            this.Controls.Add(this.labelGender);
            this.Controls.Add(this.labelFname);
            this.Controls.Add(this.labelVID);
            this.Controls.Add(this.label1);
            this.Name = "UpdateCandidate";
            this.Text = "UpdateCandidate";
            this.Load += new System.EventHandler(this.UpdateCandidate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.TextBox textBoxPname;
        private System.Windows.Forms.TextBox textBoxPID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Button buttonUpdateCan;
        private System.Windows.Forms.TextBox textBoxCContNo;
        private System.Windows.Forms.Label labelContactno;
        private System.Windows.Forms.TextBox textBoxCEmail;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.TextBox textBoxCNIC;
        private System.Windows.Forms.Label labelNIC;
        private System.Windows.Forms.ComboBox comboBoxCGender;
        private System.Windows.Forms.TextBox textBoxCFname;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelFname;
        private System.Windows.Forms.Label labelVID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCUname;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxPost;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ComboBox comboBoxCID;
    }
}
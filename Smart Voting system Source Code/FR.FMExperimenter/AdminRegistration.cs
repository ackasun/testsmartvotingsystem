﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using FlexCodeSDK;
using System.Text.RegularExpressions;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class AdminRegistration : Form
    {
       FlexCodeSDK.FinFPReg fpreg;
       String template = "";
       MySqlConnection conn = null;
       string imgPath = "";
        public AdminRegistration()
        {
            InitializeComponent();
        }

        void reg_FPRegistrationStatus(RegistrationStatus Status)
        {
            if (Status == RegistrationStatus.r_OK)
            {

                //Insert template to MySQL database
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "INSERT INTO administrator(AID,AFname,ALname,AGender,ANIC,AEmail,AContNo,AUsername,Templete) VALUES('" + textBoxAID.Text + "','" + textBoxAFname.Text + "','" + textBoxALname.Text + "','" + comboBoxAGender.SelectedItem + "','" + textBoxANIC.Text + "','" + textBoxAEmail.Text + "','" + textBoxAContNo.Text + "','" + textBoxAUname.Text + "','" + template + "')";
                cmd.ExecuteNonQuery();

                MessageBox.Show("Administrator Registered Successfully !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxAID.Text = String.Empty;
                textBoxAFname.Text = String.Empty;
                textBoxALname.Text = String.Empty;
                comboBoxAGender.Text = String.Empty;
                textBoxANIC.Text = String.Empty;
                textBoxAEmail.Text = String.Empty;
                textBoxAContNo.Text = String.Empty;
                textBoxAUname.Text = String.Empty;
                pictureBox1.Image = null;
            }
        }



        void reg_FPRegistrationTemplate(string FPTemplate)
        {
            template = FPTemplate;
        }

        void reg_FPRegistrationImage()
        {
            pictureBox1.Load(imgPath);
            if (imgPath == "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\104_1.tif")
            {
                imgPath = "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\104_2.tif";
            }
            else
            {
                imgPath = "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\104_1.tif";
            }
            fpreg.PictureSamplePath = imgPath;
        }

        void reg_FPSamplesNeeded(short Samples)
        {
            label2.Text = "Samples Needed : " + Convert.ToString(Samples);
        }

        private bool ValidateNIC(string p)
        {
            bool isMatchCase = Regex.IsMatch(p, @"^\d{9}(x|X|v|V)$");
            return isMatchCase;
        }

        private bool ValidateEmail(string w)
        {
            bool isMatchCase = Regex.IsMatch(w, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            return isMatchCase;
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            if (textBoxAID.Text == "")/*To check whether fields are empty*/
            {
                MessageBox.Show("Enter the Administrator ID", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxAID.Focus();
            }
            else if (textBoxAFname.Text == "")
            {
                MessageBox.Show("Enter the First Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxAFname.Focus();
            }
            else if (textBoxALname.Text == "")
            {
                MessageBox.Show("Enter the Last Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxALname.Focus();
            }
            else if (comboBoxAGender.Text == "")
            {
                MessageBox.Show("Select the Gender", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
            else if (textBoxANIC.Text == "")
            {
                MessageBox.Show("Enter the NIC No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxANIC.Focus();
            }
            else if (textBoxANIC.Text.Length != 10)
            {
                MessageBox.Show("Use 10 characters for NIC No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxANIC.Text = String.Empty;
                textBoxANIC.Focus();
            }
            else if (ValidateNIC(textBoxANIC.Text) == false)
            {
                MessageBox.Show("Invalid NIC Format", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxANIC.Text = String.Empty;
                textBoxANIC.Focus();
            }
            else if (textBoxAEmail.Text == "")
            {
                MessageBox.Show("Enter the Email", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxAEmail.Focus();
            }
            else if (ValidateEmail(textBoxAEmail.Text) == false)
            {
                MessageBox.Show("Invalid Email Format", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxAEmail.Text = String.Empty;
                textBoxAEmail.Focus();
            }
            else if (textBoxAContNo.Text == "")
            {
                MessageBox.Show("Enter the Contact No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxAContNo.Focus();
            }
            else if (textBoxAContNo.Text.Length != 10)
            {
                MessageBox.Show("Use 10 characters for Contact No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxAContNo.Text = String.Empty;
                textBoxAContNo.Focus();
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(textBoxAContNo.Text, "[^0-9]"))
            {
                MessageBox.Show("Invalid contact No Format", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxAContNo.Text = String.Empty;
                textBoxAContNo.Focus();
            }
            else if (textBoxAUname.Text == "")
            {
                MessageBox.Show("Enter the User Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxAUname.Focus();
            }

            else
            {
                DBLayer.DBConnection obj1 = new DBLayer.DBConnection();
                MySqlCommand da = new MySqlCommand("select count(*)from administrator where AID='" + textBoxAID.Text + "'", obj1.createConnection());
                int count = Convert.ToInt32(da.ExecuteScalar());


                if (count > 0)
                {
                    MessageBox.Show("This Administrator ID is already in the database. Please use another ID !!", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                else
                {
                    fpreg.FPRegistrationStart("MySecretKey" + textBoxAID.Text);
                }
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxAID.Text = String.Empty;
            textBoxAFname.Text = String.Empty;
            textBoxALname.Text = String.Empty;
            comboBoxAGender.Text = String.Empty;
            textBoxANIC.Text = String.Empty;
            textBoxAEmail.Text = String.Empty;
            textBoxAContNo.Text = String.Empty;
            textBoxAUname.Text = String.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            AdminPanel obj = new AdminPanel();
            this.Hide();
            obj.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void AdminRegistration_Load(object sender, EventArgs e)
        {
            // Set MySQL database connection
            string cs = "server=localhost;userid=root;password=;database=icbtelection";
            conn = new MySqlConnection(cs);
            conn.Open();

            //Initialize FlexCodeSDK for Registration
            //1. Initialize Event Handler
            fpreg = new FlexCodeSDK.FinFPReg();
            fpreg.FPSamplesNeeded += new __FinFPReg_FPSamplesNeededEventHandler(reg_FPSamplesNeeded);
            fpreg.FPRegistrationTemplate += new __FinFPReg_FPRegistrationTemplateEventHandler(reg_FPRegistrationTemplate);
            fpreg.FPRegistrationImage += new __FinFPReg_FPRegistrationImageEventHandler(reg_FPRegistrationImage);
            fpreg.FPRegistrationStatus += new __FinFPReg_FPRegistrationStatusEventHandler(reg_FPRegistrationStatus);

            //2. Input the activation code
            fpreg.AddDeviceInfo("44037546", "FE8C5A406D7F6DB", "ALD5BB724DF16EACB38D1YB4");

            //3. Define fingerprint image
            fpreg.PictureSampleHeight = (short)(pictureBox1.Height * 20); //FlexCodeSDK use Twips. 1 pixel = 15 twips
            fpreg.PictureSampleWidth = (short)(pictureBox1.Width * 20); //FlexCodeSDK use Twips. 1 pixel = 15 twips
            imgPath = "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\104_1.tif";
            fpreg.PictureSamplePath = imgPath;
        }
    }
}

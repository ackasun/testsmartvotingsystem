﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class UpdateAdmin : Form
    {
        public UpdateAdmin()
        {
            InitializeComponent();
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
        }
            

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            //Create windowsDBLayer Registration object
            DBLayer.Admin obj = new DBLayer.Admin();
            obj.AID1 = comboBoxAID.SelectedItem.ToString();
            obj.AFname1 = textBoxAFname.Text.Trim();
            obj.ALname1 = textBoxALname.Text.Trim();
            obj.AGender1 = comboBoxAGender.SelectedItem.ToString();
            obj.ANIC1 = textBoxANIC.Text.Trim();
            obj.AEmail1 = textBoxAEmail.Text.Trim();
            obj.AContNo1 = textBoxAContNo.Text.Trim();
            obj.AUname1 = textBoxAUname.Text.Trim();

            obj.UpdateAdmin(obj);
            //output message if update is successfull
            MessageBox.Show("Administrator information Updated Successfully !!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            comboBoxAID.Text = String.Empty;
            textBoxAFname.Text = String.Empty;
            textBoxALname.Text = String.Empty;
            comboBoxAGender.Text = String.Empty;
            textBoxANIC.Text = String.Empty;
            textBoxAEmail.Text = String.Empty;
            textBoxAContNo.Text = String.Empty;
            textBoxAUname.Text = String.Empty;
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            //Create windowsDBLayer Registration object
            DBLayer.Admin obj = new DBLayer.Admin();

            obj.AID1 = comboBoxAID.SelectedItem.ToString();


            obj.DeleteAdmin(obj);
            //output message if update is successfull
            MessageBox.Show("Administrator information Removed Successfully !!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            comboBoxAID.Text = String.Empty;
            textBoxAFname.Text = String.Empty;
            textBoxALname.Text = String.Empty;
            comboBoxAGender.Text = String.Empty;
            textBoxANIC.Text = String.Empty;
            textBoxAEmail.Text = String.Empty;
            textBoxAContNo.Text = String.Empty;
            textBoxAUname.Text = String.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            AdminPanel obj = new AdminPanel();
            this.Hide();
            obj.Show();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            comboBoxAID.Text = String.Empty;
            textBoxAFname.Text = String.Empty;
            textBoxALname.Text = String.Empty;
            comboBoxAGender.Text = String.Empty;
            textBoxANIC.Text = String.Empty;
            textBoxAEmail.Text = String.Empty;
            textBoxAContNo.Text = String.Empty;
            textBoxAUname.Text = String.Empty;
            
        }

        private void UpdateAdmin_Load(object sender, EventArgs e)
        {
            try
            {


                MySqlConnection mcon = new MySqlConnection(@"server=localhost;database=icbtelection;username=root;password=;");
                string s = "SELECT * from administrator";

                mcon.Open();
                MySqlCommand mcd = new MySqlCommand(s, mcon);
                MySqlDataReader mdr = mcd.ExecuteReader();
                while (mdr.Read())
                {

                    comboBoxAID.Items.Add(mdr.GetString("AID"));

                }
                mcon.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void comboBoxAID_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String constring = "server=localhost;database=icbtelection;username=root;password=;";
                String Queary = "SELECT * from administrator WHERE AID='" + comboBoxAID.Text + "'";
                MySqlConnection conDatabase = new MySqlConnection(constring);
                MySqlCommand cmdDatabase = new MySqlCommand(Queary, conDatabase);
                MySqlDataReader rd;
                try
                {

                    conDatabase.Open();
                    rd = cmdDatabase.ExecuteReader();
                    while (rd.Read())
                    {
                        textBoxAFname.Text = (String)rd["AFname"];
                        textBoxALname.Text = (String)rd["ALname"];
                        comboBoxAGender.Text = (String)rd["AGender"];
                        textBoxANIC.Text = (String)rd["ANIC"];
                        textBoxAEmail.Text = (String)rd["AEmail"];
                        textBoxAContNo.Text = (String)rd["AContNo"];
                        textBoxAUname.Text = (String)rd["AUsername"];
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

    }
}

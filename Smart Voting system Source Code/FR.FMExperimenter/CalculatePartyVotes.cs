﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class CalculatePartyVotes : Form
    {
        public CalculatePartyVotes()
        {
            InitializeComponent();
        }

      
        private void CalculatePartyVotes_Load(object sender, EventArgs e)
        {
            try
            {


                MySqlConnection mcon = new MySqlConnection(@"server=localhost;database=icbtelection;username=root;password=;");
                string s = "SELECT * from candidate";

                mcon.Open();
                MySqlCommand mcd = new MySqlCommand(s, mcon);
                MySqlDataReader mdr = mcd.ExecuteReader();
                while (mdr.Read())
                {

                    comboBoxParty.Items.Add(mdr.GetString("Pname"));

                }
                mcon.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBoxCandidateName_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void comboBoxCandidateName_SelectedIndexChanged_1(object sender, EventArgs e)
        {
          
            String constring = "server=localhost;database=icbtelection;username=root;password=;";
            MySqlConnection mycon = new MySqlConnection(constring);
            MySqlCommand cmd = new MySqlCommand("select CFname,Pname,NoOfVotes from candidate where Pname='" + comboBoxParty.Text + "'", mycon);
            try
            {

                MySqlDataAdapter adp = new MySqlDataAdapter();
                adp.SelectCommand = cmd;
                DataTable table = new DataTable();
                adp.Fill(table);
                BindingSource source = new BindingSource();
                source.DataSource = table;

                dataGridView1.DataSource = source;
                adp.Update(table);



        
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void ButtonSendEmail_Click(object sender, EventArgs e)
        {
            if (comboBoxParty.Text == "")
            {
                MessageBox.Show("Select a Party Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                int sum = 0;
                String Pname = comboBoxParty.SelectedItem.ToString();
                for (int i = 0; i < dataGridView1.Rows.Count; ++i)
                {
                    sum += Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value);
                    textBoxvotes.Text = sum.ToString();
                }
                MessageBox.Show(Pname + " has got " + sum + " Votes in the ICBT Student Council Election !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            comboBoxParty.Text = String.Empty;
            textBoxvotes.Text = String.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            AdminPanel obj = new AdminPanel();
            this.Hide();
            obj.Show();
        }

        private void buttonReports_Click(object sender, EventArgs e)
        {
            PartyResults obj = new PartyResults();
            obj.Show();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DBLayer;
using MySql.Data.MySqlClient;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class PartyRegistration : Form
    {
        public PartyRegistration()
        {
            InitializeComponent();
        }
        MySqlConnection Connection = new MySqlConnection("server=localhost;database=icbtelection;username=root;password=;");
        MySqlCommand Command;
        private void label6_Click(object sender, EventArgs e)
        {
           
        }

        private void textBoxVID_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxGender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void PartyRegistration_Load(object sender, EventArgs e)
        {

        }

        private void buttonSelectSymbol_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
           
            opf.Filter = "jpg files (*.jpg)|*.jpg";
            if (opf.ShowDialog() == DialogResult.OK)
            {
                pictureBoxSymbol.Image = System.Drawing.Image.FromFile(opf.FileName);
            }
            opf.Dispose();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void labelLName_Click(object sender, EventArgs e)
        {

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            AdminPanel obj = new AdminPanel();
            this.Hide();
            obj.Show();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxPID.Text = String.Empty;
            textBoxPname.Text = String.Empty;
            textBoxLeader.Text = String.Empty;
            comboBoxNoCandi.Text = String.Empty;
            pictureBoxSymbol.Image = null;
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            if (textBoxPID.Text == "")/*To check whether fields are empty*/
            {
                MessageBox.Show("Enter the Party ID", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxPID.Focus();
            }
            else if (textBoxPname.Text == "")
            {
                MessageBox.Show("Enter the Party Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxPname.Focus();
            }
            else if (textBoxLeader.Text == "")
            {
                MessageBox.Show("Enter the Party Leader", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxLeader.Focus();
            }
            else if (comboBoxNoCandi.Text == "")
            {
                MessageBox.Show("Enter the No Of Candidates", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }

            else
            {
                DBLayer.DBConnection obj1 = new DBLayer.DBConnection();
                MySqlCommand da = new MySqlCommand("select count(*)from party where PID='" + textBoxPID.Text + "'", obj1.createConnection());
                int count = Convert.ToInt32(da.ExecuteScalar());

              
                if (count > 0)
                {
                    MessageBox.Show("This Party ID is already in the database. Please use another ID !!", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
               
                else
                {
                    MemoryStream ms = new MemoryStream();
                    pictureBoxSymbol.Image.Save(ms, pictureBoxSymbol.Image.RawFormat);
                    byte[] img = ms.ToArray();

                    MySqlParameter[] parameters = new MySqlParameter[5];

                    parameters[0] = new MySqlParameter("PID", MySqlDbType.VarChar, 20);
                    parameters[0].Value = textBoxPID.Text;

                    parameters[1] = new MySqlParameter("Pname", MySqlDbType.VarChar, 50);
                    parameters[1].Value = textBoxPname.Text;

                    parameters[2] = new MySqlParameter("PLeader", MySqlDbType.VarChar, 20);
                    parameters[2].Value = textBoxLeader.Text;

                    parameters[3] = new MySqlParameter("NoOfCandidates", MySqlDbType.VarChar, 20);
                    parameters[3].Value = comboBoxNoCandi.Text;

                    parameters[4] = new MySqlParameter("ImagePath", MySqlDbType.LongBlob);
                    parameters[4].Value = img;

                    Command = new MySqlCommand();
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandText = "Add_Party";
                    Command.Connection = Connection;
                    Command.Parameters.AddRange(parameters);
                    Connection.Open();
                    Command.ExecuteNonQuery();
                    MessageBox.Show("Party Registered successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Connection.Close();
                    textBoxPID.Text = String.Empty;
                    textBoxPname.Text = String.Empty;
                    textBoxLeader.Text = String.Empty;
                    comboBoxNoCandi.Text = String.Empty;
                    pictureBoxSymbol.Image = null;

                }
            }          
        }
    }
}

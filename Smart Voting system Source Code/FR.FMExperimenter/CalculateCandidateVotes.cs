﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using DBLayer;
using BLLayer;


namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class CalculateCandidateVotes : Form
    {
        public CalculateCandidateVotes()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void CalculateVotes_Load(object sender, EventArgs e)
        {
            try
            {


                MySqlConnection mcon = new MySqlConnection(@"server=localhost;database=icbtelection;username=root;password=;");
                string s = "SELECT * from candidate WHERE Post='President'";

                mcon.Open();
                MySqlCommand mcd = new MySqlCommand(s, mcon);
                MySqlDataReader mdr = mcd.ExecuteReader();
                while (mdr.Read())
                {

                    comboBoxCandidateName.Items.Add(mdr.GetString("CFname"));

                }
                mcon.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBoxCandidateName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                String constring = "server=localhost;database=icbtelection;username=root;password=;";
                String Queary = "SELECT * from candidate WHERE CFname='" + comboBoxCandidateName.Text + "'";
                MySqlConnection conDatabase = new MySqlConnection(constring);
                MySqlCommand cmdDatabase = new MySqlCommand(Queary, conDatabase);
                MySqlDataReader myReader;
                try
                {

                    conDatabase.Open();
                    myReader = cmdDatabase.ExecuteReader();
                    while (myReader.Read())
                    {
                        textBoxPname.Text = myReader.GetString("Pname");
                        textBoxEmail.Text = myReader.GetString("CEmail");
                        textBoxPost.Text = myReader.GetString("Post");
                        textBoxTotalVotes.Text = myReader.GetString("NoOfVotes");
                        byte[] img = (byte[])(myReader["Image"]);
                        MemoryStream mstream = new MemoryStream(img);
                        pictureBoxImage.Image = System.Drawing.Image.FromStream(mstream);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

      

        private void ButtonSendEmail_Click(object sender, EventArgs e)
        {
            if (comboBoxCandidateName.Text == "")
            {
                MessageBox.Show("Select a Candidate Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                BLLayer.SendEmail obj = new BLLayer.SendEmail();
                obj.Email1 = textBoxEmail.Text.Trim();
                obj.CandidateName1 = comboBoxCandidateName.Text.Trim();
                obj.Post1 = textBoxPost.Text.Trim();
                obj.TotalVotes1 = textBoxTotalVotes.Text;

                obj.Sendmail(obj);
                //output message if update is successfull
                MessageBox.Show("Email Sent successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                comboBoxCandidateName.Text = String.Empty;
                textBoxPname.Text = String.Empty;
                textBoxEmail.Text = String.Empty;
                textBoxPost.Text = String.Empty;
                textBoxTotalVotes.Text = String.Empty;
                pictureBoxImage.Image = null;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AdminPanel obj = new AdminPanel();
            this.Hide();
            obj.Show();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            comboBoxCandidateName.Text = String.Empty;
            textBoxPname.Text = String.Empty;
            textBoxEmail.Text = String.Empty;
            textBoxPost.Text = String.Empty;
            textBoxTotalVotes.Text = String.Empty;
            pictureBoxImage.Image = null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FlexCodeSDK;
using MySql.Data.MySqlClient;
using System.IO;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class VoterRegistration : Form
    {
        FlexCodeSDK.FinFPReg fpreg;
        String template = "";
        MySqlConnection conn = null;
        string imgPath = "";
        public VoterRegistration()
        {
            InitializeComponent();
        }

        void reg_FPRegistrationStatus(RegistrationStatus Status)
        {
            if (Status == RegistrationStatus.r_OK)
            {
              
                //Insert template to MySQL database
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "INSERT INTO voter(VID,Fname,Lname,Gender,NIC,Email,ContNo,Username,Templete) VALUES('" + textBoxVID.Text + "','" + textBoxFname.Text + "','" + textBoxLname.Text + "','" + comboBoxGender.SelectedItem + "','" + textBoxNIC.Text + "','" + textBoxEmail.Text + "','" + textBoxContNo.Text + "','" + textBoxVUname.Text + "','" + template + "')";
                cmd.ExecuteNonQuery();

                MessageBox.Show("Voter Registered Successfully !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxVID.Text = String.Empty;
                textBoxFname.Text = String.Empty;
                textBoxLname.Text = String.Empty;
                comboBoxGender.Text = String.Empty;
                textBoxNIC.Text = String.Empty;
                textBoxEmail.Text = String.Empty;
                textBoxContNo.Text = String.Empty;
                textBoxVUname.Text = String.Empty;
                pictureBox1.Image = null;
            }
        }

   

        void reg_FPRegistrationTemplate(string FPTemplate)
        {
            template = FPTemplate;
        }

        void reg_FPRegistrationImage()
        {
            pictureBox1.Load(imgPath);
            if (imgPath == "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\102_1.tif")
            {
                imgPath = "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\102_2.tif";
            }
            else
            {
                imgPath = "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\102_1.tif";
            }
            fpreg.PictureSamplePath = imgPath;
        }

        void reg_FPSamplesNeeded(short Samples)
        {
            label2.Text = "Samples Needed : " + Convert.ToString(Samples);
        }

        private bool ValidateNIC(string p)
        {
            bool isMatchCase = Regex.IsMatch(p, @"^\d{9}(x|X|v|V)$");
            return isMatchCase;
        }

        private bool ValidateEmail(string w)
        {
            bool isMatchCase = Regex.IsMatch(w, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            return isMatchCase;
        }
        private void buttonRegister_Click(object sender, EventArgs e)
        {
             if (textBoxVID.Text == "")/*To check whether fields are empty*/
            {
                MessageBox.Show("Enter the Voter ID", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxVID.Focus();
            }
            else if (textBoxFname.Text == "")
            {
                MessageBox.Show("Enter the First Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxFname.Focus();
            }
            else if (textBoxLname.Text == "")
            {
                MessageBox.Show("Enter the Last Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxLname.Focus();
            }
             else if (comboBoxGender.Text == "")
             {
                 MessageBox.Show("Select the Gender", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);

             }
             else if (textBoxNIC.Text == "")
             {
                 MessageBox.Show("Enter the NIC No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxNIC.Focus();
             }
             else if (textBoxNIC.Text.Length!=10)
             {
                 MessageBox.Show("Use 10 characters for NIC No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxNIC.Text = String.Empty;
                 textBoxNIC.Focus();
             }
             else if (ValidateNIC(textBoxNIC.Text) == false)
             {
                 MessageBox.Show("Invalid NIC Format", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxNIC.Text = String.Empty;
                 textBoxNIC.Focus();
             }
             else if (textBoxEmail.Text == "")
             {
                 MessageBox.Show("Enter the Email", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxEmail.Focus();
             }
             else if (ValidateEmail(textBoxEmail.Text) == false)
             {
                 MessageBox.Show("Invalid Email Format", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxEmail.Text = String.Empty;
                 textBoxEmail.Focus();
             }
             else if (textBoxContNo.Text == "")
             {
                 MessageBox.Show("Enter the Contact No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxContNo.Focus();
             }
             else if (textBoxContNo.Text.Length != 10)
             {
                 MessageBox.Show("Use 10 characters for Contact No", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxContNo.Text = String.Empty;
                 textBoxContNo.Focus();
             }
             else if (System.Text.RegularExpressions.Regex.IsMatch(textBoxContNo.Text, "[^0-9]"))
             {
                 MessageBox.Show("Invalid contact No Format", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxContNo.Text = String.Empty;
                 textBoxContNo.Focus();
             }
             else if (textBoxVUname.Text == "")
             {
                 MessageBox.Show("Enter the User Name", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBoxVUname.Focus();
             }

             else
             {
                 DBLayer.DBConnection obj1 = new DBLayer.DBConnection();
                 MySqlCommand da = new MySqlCommand("select count(*)from voter where VID='" + textBoxVID.Text + "'", obj1.createConnection());
                 int count = Convert.ToInt32(da.ExecuteScalar());


                 if (count > 0)
                 {
                     MessageBox.Show("This Voter ID is already in the database. Please use another ID !!", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 }

                 else
                 {
                     fpreg.FPRegistrationStart("MySecretKey" + textBoxVID.Text);
                    
                 }
             }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            AdminPanel obj = new AdminPanel();
            this.Hide();
            obj.Show();

        }

        private void labelVID_Click(object sender, EventArgs e)
        {

        }

        private void textBoxVID_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelLName_Click(object sender, EventArgs e)
        {

        }

        private void labelFname_Click(object sender, EventArgs e)
        {

        }

        private void labelGender_Click(object sender, EventArgs e)
        {

        }

        private void textBoxFname_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxLname_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxGender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void labelNIC_Click(object sender, EventArgs e)
        {

        }

        private void textBoxNIC_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelEmail_Click(object sender, EventArgs e)
        {

        }

        private void textBoxEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelContactno_Click(object sender, EventArgs e)
        {

        }

        private void textBoxContNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxVID.Text = String.Empty;
            textBoxFname.Text = String.Empty;
            textBoxLname.Text = String.Empty;
            comboBoxGender.Text = String.Empty;
            textBoxNIC.Text = String.Empty;
            textBoxEmail.Text = String.Empty;
            textBoxContNo.Text = String.Empty;
            textBoxVUname.Text = String.Empty;
            pictureBox1.Image = null;
        }

        private void VoterRegistration_Load(object sender, EventArgs e)
        {
            // Set MySQL database connection
            string cs = "server=localhost;userid=root;password=;database=icbtelection";
            conn = new MySqlConnection(cs);
            conn.Open();

            //Initialize FlexCodeSDK for Registration
            //1. Initialize Event Handler
            fpreg = new FlexCodeSDK.FinFPReg();
            fpreg.FPSamplesNeeded += new __FinFPReg_FPSamplesNeededEventHandler(reg_FPSamplesNeeded);
            fpreg.FPRegistrationTemplate += new __FinFPReg_FPRegistrationTemplateEventHandler(reg_FPRegistrationTemplate);
            fpreg.FPRegistrationImage += new __FinFPReg_FPRegistrationImageEventHandler(reg_FPRegistrationImage);
            fpreg.FPRegistrationStatus += new __FinFPReg_FPRegistrationStatusEventHandler(reg_FPRegistrationStatus);

            //2. Input the activation code
            fpreg.AddDeviceInfo("44037546", "FE8C5A406D7F6DB", "ALD5BB724DF16EACB38D1YB4");

            //3. Define fingerprint image
            fpreg.PictureSampleHeight = (short)(pictureBox1.Height * 20); //FlexCodeSDK use Twips. 1 pixel = 15 twips
            fpreg.PictureSampleWidth = (short)(pictureBox1.Width * 20); //FlexCodeSDK use Twips. 1 pixel = 15 twips
            imgPath = "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db1\\102_1.tif";
            fpreg.PictureSamplePath = imgPath;
        }

        private void textBoxVUname_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using PatternRecognition.FingerprintRecognition.Core;
using PatternRecognition.FingerprintRecognition.FeatureDisplay;
using MySql.Data.MySqlClient;
using FlexCodeSDK;
using DBLayer;


namespace PatternRecognition.FingerprintRecognition.Applications
{
    public partial class VisualMatchingForm : Form
    {
        FlexCodeSDK.FinFPReg fpreg;
        String templete = "";
        String DisplayName="";
        string imgPath = "";
        public VisualMatchingForm()
        {
            InitializeComponent();
        }

        //tepmplete registration
        void reg_FPRegistrationTemplate(string FPTemplate)
        {
            templete = FPTemplate;
        }

        //finger print images saving method
        void reg_FPRegistrationImage()
        {
            pbxQueryImg.Load(imgPath);
            string pathToSave = @"F:\Degree Projects\Final Project\Smart Vorting System\Smart Voting system Source Code\bin\Debug\db\";
            imgPath = pathToSave+"101_1"+ DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")+".tif";
            fpreg.PictureSamplePath = imgPath;
        }

        //finger print samples counting
        void reg_FPSamplesNeeded(short Samples)
        {
            label1.Text = "Samples Needed : " + Convert.ToString(Samples);
        }


         private void VisualMatchingForm_Load(object sender, EventArgs e)
        {
           

            //Initialize FlexCodeSDK for Registration
            // Initialize Event Handler
            fpreg = new FlexCodeSDK.FinFPReg();
            fpreg.FPSamplesNeeded += new __FinFPReg_FPSamplesNeededEventHandler(reg_FPSamplesNeeded);
            fpreg.FPRegistrationTemplate += new __FinFPReg_FPRegistrationTemplateEventHandler(reg_FPRegistrationTemplate);
            fpreg.FPRegistrationImage += new __FinFPReg_FPRegistrationImageEventHandler(reg_FPRegistrationImage);
         

            // Input the activation code
            fpreg.AddDeviceInfo("44037546", "FE8C5A406D7F6DB", "ALD5BB724DF16EACB38D1YB4");

            // Define fingerprint image
            fpreg.PictureSampleHeight = (short)(pbxQueryImg.Height * 20); //FlexCodeSDK use Twips. 1 pixel = 15 twips
            fpreg.PictureSampleWidth = (short)(pbxQueryImg.Width * 20); //FlexCodeSDK use Twips. 1 pixel = 15 twips
            imgPath = "F:\\Degree Projects\\Final Project\\Smart Vorting System\\Smart Voting system Source Code\\bin\\Debug\\db\\101_1.tif";
            fpreg.PictureSamplePath = imgPath;
        }

        #region private fields

        private Bitmap qImage, tImage;

        private IResourceProvider provider;

        private ResourceRepository repository;

        private string resourcePath;

        private IMatcher matcher;

        private object qFeatures, tFeatures;

        #endregion



        public VisualMatchingForm(IMatcher matcher, IResourceProvider resourceProvider, string resourcePath)
        {
            InitializeComponent();
            this.matcher = matcher;
            provider = resourceProvider;
            this.resourcePath = resourcePath;
            repository = new ResourceRepository(resourcePath);
        }

        //results displaying method
        private void ShowResults(double matchingScore, List<MinutiaPair> matchingMtiae)
        {
            if (matchingScore == 0 || matchingMtiae == null)
                MessageBox.Show(string.Format("Similarity: {0}.", matchingScore));
            else
            {
                List<Minutia> qMtiae = new List<Minutia>();
                List<Minutia> tMtiae = new List<Minutia>();
                foreach (MinutiaPair mPair in matchingMtiae)
                {
                    qMtiae.Add(mPair.QueryMtia);
                    tMtiae.Add(mPair.TemplateMtia);
                }
                IFeatureDisplay<List<Minutia>> display = new MinutiaeDisplay();

                pbxQueryImg.Image = qImage.Clone() as Bitmap;
                Graphics g = Graphics.FromImage(pbxQueryImg.Image);
                display.Show(qMtiae, g);
                pbxQueryImg.Invalidate();

                pictureBox1.Image = tImage.Clone() as Bitmap;
                g = Graphics.FromImage(pictureBox1.Image);
                display.Show(tMtiae, g);
                pictureBox1.Invalidate();

            
            }
        }

        public void ShowBlueMinutiae(List<Minutia> features, Graphics g)
        {
            int mtiaRadius = 6;
            int lineLength = 18;
            Pen pen = new Pen(Brushes.Blue) { Width = 3 };
            pen.Color = Color.LightBlue;

            Pen whitePen = new Pen(Brushes.Blue) { Width = 5 };
            whitePen.Color = Color.White;

            int i = 0;
            foreach (Minutia mtia in (IList<Minutia>)features)
            {
                g.DrawEllipse(whitePen, mtia.X - mtiaRadius, mtia.Y - mtiaRadius, 2 * mtiaRadius + 1, 2 * mtiaRadius + 1);
                g.DrawLine(whitePen, mtia.X, mtia.Y, Convert.ToInt32(mtia.X + lineLength * Math.Cos(mtia.Angle)), Convert.ToInt32(mtia.Y + lineLength * Math.Sin(mtia.Angle)));

                pen.Color = Color.LightBlue;

                g.DrawEllipse(pen, mtia.X - mtiaRadius, mtia.Y - mtiaRadius, 2 * mtiaRadius + 1, 2 * mtiaRadius + 1);
                g.DrawLine(pen, mtia.X, mtia.Y, Convert.ToInt32(mtia.X + lineLength * Math.Cos(mtia.Angle)), Convert.ToInt32(mtia.Y + lineLength * Math.Sin(mtia.Angle)));
                i++;
            }

            Minutia lastMtia = ((IList<Minutia>)features)[((IList<Minutia>)features).Count - 1];
            pen.Color = Color.LightBlue;
            g.DrawEllipse(pen, lastMtia.X - mtiaRadius, lastMtia.Y - mtiaRadius, 2 * mtiaRadius + 1, 2 * mtiaRadius + 1);
            g.DrawLine(pen, lastMtia.X, lastMtia.Y, Convert.ToInt32(lastMtia.X + lineLength * Math.Cos(lastMtia.Angle)), Convert.ToInt32(lastMtia.Y + lineLength * Math.Sin(lastMtia.Angle)));
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
          
            //if-else conditions to select the user type
               if (comboBoxUsertype.Text == "Administrator")
               {
                   label5.Text = "Matched finger print";

                   if (qImage == null)
                   {
                       MessageBox.Show("Finger print nedded. Please load a finger print !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                       return;
                   }

                   // Matching features
                   List<MinutiaPair> matchingMtiae = null;
                   double score;
                   IMinutiaMatcher minutiaMatcher = matcher as IMinutiaMatcher;
                   if (minutiaMatcher != null)
                   {
                       for (int y = 101; y < 103; y++)
                       {
                           for (int i = 0; i < 1; i++)     // matching each prints using a for loop
                           {
                               string shortFileName = Path.GetFileNameWithoutExtension(resourcePath + "\\" + y + "_" + (i + 1) + ".tif");
                               try
                               {
                                   tFeatures = provider.GetResource(shortFileName, repository);
                               }
                               catch (Exception)
                               {

                                   throw;
                               }
                               openFileDialog1.FileName = resourcePath + "\\";
                               tImage = ImageLoader.LoadImage(openFileDialog1.FileName + y + "_" + (i + 1) + ".tif");
                               pictureBox1.Image = tImage;

                               score = minutiaMatcher.Match(qFeatures, tFeatures, out matchingMtiae);  // finger print matching algorithm
                               if (score > 50.00 && score <= 100.0)
                               {
                                   y = 103;
                                   label3.Text = shortFileName + " - Finger Print detected";

                                   MessageBox.Show(string.Format("Similarity: {0}. Matching minutiae: {1}.", score, matchingMtiae.Count), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                   MessageBox.Show("Administrator Login Successfully !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                   // display the Admin panel
                                   AdminPanel obj = new AdminPanel();
                                   this.Hide();
                                   obj.Show();
                                   break;

                               }
                               else
                               {
                                   label3.Text = "Finger Print is not matching. Access Denied !!";
                                   MessageBox.Show("Finger Print is not matching. Access Denied !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                               }


                           }
                       }

                       score = minutiaMatcher.Match(qFeatures, tFeatures, out matchingMtiae);  // matching algorithm


                       if (qFeatures is List<Minutia> && tFeatures is List<Minutia>)
                       {
                           pbxQueryImg.Image = qImage.Clone() as Bitmap;
                           Graphics g1 = Graphics.FromImage(pbxQueryImg.Image);
                           ShowBlueMinutiae(qFeatures as List<Minutia>, g1);
                        

                           pictureBox1.Image = tImage.Clone() as Bitmap;
                           Graphics g2 = Graphics.FromImage(pictureBox1.Image);
                           ShowBlueMinutiae(tFeatures as List<Minutia>, g2);
                        

                           if (score == 0 || matchingMtiae == null)
                               MessageBox.Show(string.Format("Similarity: {0}.", score));
                           else
                           {
                               List<Minutia> qMtiae = new List<Minutia>();
                               List<Minutia> tMtiae = new List<Minutia>();
                               foreach (MinutiaPair mPair in matchingMtiae)
                               {
                                   qMtiae.Add(mPair.QueryMtia);
                                   tMtiae.Add(mPair.TemplateMtia);
                               }
                               IFeatureDisplay<List<Minutia>> display = new MinutiaeDisplay();

                               display.Show(qMtiae, g1);
                               pbxQueryImg.Invalidate();

                               display.Show(tMtiae, g2);
                               pictureBox1.Invalidate();

                               
                           }
                       }
                       else
                           ShowResults(score, matchingMtiae);

                   }
                   else
                       score = matcher.Match(qFeatures, tFeatures);

               }

               else if (comboBoxUsertype.Text=="Voter")
               {
                   
                   DBLayer.Login obj = new DBLayer.Login(); //To access Login class properties 
                   MySqlDataReader reader = obj.getVoterLogin(textBoxUname.Text);//To check whether username is valid
                   while (reader.Read())//while loop to retrieve user information
                    {
                           DisplayName = (String)reader["Fname"];
                           label5.Text = "Matched finger print";

                           if (qImage == null)
                           {
                               MessageBox.Show("Finger print nedded. Please load a finger print !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                               return;
                           }

                           // Matching features
                           List<MinutiaPair> matchingMtiae = null;
                           double score;
                           IMinutiaMatcher minutiaMatcher = matcher as IMinutiaMatcher;



                           if (minutiaMatcher != null)
                           {
                               for (int y = 101; y < 103; y++)
                               {


                                   for (int i = 0; i < 1; i++)     // matching each prints using for loop
                                   {
                                       string shortFileName = Path.GetFileNameWithoutExtension(resourcePath + "\\" + y + "_" + (i + 1) + ".tif");
                                       try
                                       {
                                           tFeatures = provider.GetResource(shortFileName, repository);
                                       }
                                       catch (Exception)
                                       {

                                           throw;
                                       }
                                       openFileDialog1.FileName = resourcePath + "\\";
                                       tImage = ImageLoader.LoadImage(openFileDialog1.FileName + y + "_" + (i + 1) + ".tif");
                                       pictureBox1.Image = tImage;

                                       score = minutiaMatcher.Match(qFeatures, tFeatures, out matchingMtiae);  // matching algorithm
                                       if (score > 50.00 && score <= 100.0)
                                       {
                                           y = 103;
                                           label3.Text = shortFileName + " - Finger Print detected";
                                           MessageBox.Show(string.Format("Similarity: {0}. Matching minutiae: {1}.", score, matchingMtiae.Count), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                           MessageBox.Show(DisplayName+" has Succesfully logged in as a Voter !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                           //Display the Vote Casting interface
                                           VotingGuide myobj = new VotingGuide();
                                           this.Hide();
                                           myobj.Show();
                                           break;

                                       }
                                       else
                                       {
                                           label3.Text = "Finger Print is not matching. Access Denied !!";
                                           MessageBox.Show("Finger Print is not matching. Access Denied !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                           

                                       }


                                   }
                               }
                               score = minutiaMatcher.Match(qFeatures, tFeatures, out matchingMtiae);  // matching algorithm


                               if (qFeatures is List<Minutia> && tFeatures is List<Minutia>)
                               {
                                   pbxQueryImg.Image = qImage.Clone() as Bitmap;
                                   Graphics g1 = Graphics.FromImage(pbxQueryImg.Image);
                                   ShowBlueMinutiae(qFeatures as List<Minutia>, g1);


                                   pictureBox1.Image = tImage.Clone() as Bitmap;
                                   Graphics g2 = Graphics.FromImage(pictureBox1.Image);
                                   ShowBlueMinutiae(tFeatures as List<Minutia>, g2);

                                   if (score == 0 || matchingMtiae == null)
                                       MessageBox.Show(string.Format("Similarity: {0}.", score));
                                   else
                                   {
                                       List<Minutia> qMtiae = new List<Minutia>();
                                       List<Minutia> tMtiae = new List<Minutia>();
                                       foreach (MinutiaPair mPair in matchingMtiae)
                                       {
                                           qMtiae.Add(mPair.QueryMtia);
                                           tMtiae.Add(mPair.TemplateMtia);
                                       }
                                       IFeatureDisplay<List<Minutia>> display = new MinutiaeDisplay();

                                       display.Show(qMtiae, g1);
                                       pbxQueryImg.Invalidate();

                                       display.Show(tMtiae, g2);
                                       pictureBox1.Invalidate();

                                       MessageBox.Show(string.Format("Similarity: {0}. Matching minutiae: {1}.", score,
                                                                     matchingMtiae.Count));
                                   }
                               }
                               else
                                   ShowResults(score, matchingMtiae);

                           }
                           else
                               score = matcher.Match(qFeatures, tFeatures);
                  }

               }
            else if (comboBoxUsertype.Text== "Candidate")
            {
                DBLayer.Login obj = new DBLayer.Login(); //To access Login class properties 
                MySqlDataReader reader1 = obj.getCandidateLogin(textBoxUname.Text);//To check whether username is valid
                while (reader1.Read())//while loop to retrieve user information
                {
                    DisplayName = (String)reader1["CFname"];
                    label5.Text = "Matched finger print";

                    if (qImage == null)
                    {
                        MessageBox.Show("Finger print nedded. Please load a finger print !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    // Matching features
                    List<MinutiaPair> matchingMtiae = null;
                    double score;
                    IMinutiaMatcher minutiaMatcher = matcher as IMinutiaMatcher;



                    if (minutiaMatcher != null)
                    {
                        for (int y = 101; y < 103; y++)
                        {


                            for (int i = 0; i < 1; i++)     // matching each prints using for loop
                            {
                                string shortFileName = Path.GetFileNameWithoutExtension(resourcePath + "\\" + y + "_" + (i + 1) +".tif");
                                try
                                {
                                    tFeatures = provider.GetResource(shortFileName, repository);
                                }
                                catch (Exception)
                                {

                                    throw;
                                }
                                openFileDialog1.FileName = resourcePath + "\\";
                                tImage = ImageLoader.LoadImage(openFileDialog1.FileName + y + "_" + (i + 1) + ".tif");
                                pictureBox1.Image = tImage;

                                score = minutiaMatcher.Match(qFeatures, tFeatures, out matchingMtiae);  // matching algorithm
                                if (score > 98.00 && score <= 100.0)
                                {
                                    y = 103;
                                    label3.Text = shortFileName + " - Finger Print detected";
                                    MessageBox.Show(string.Format("Similarity: {0}. Matching minutiae: {1}.", score, matchingMtiae.Count), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    MessageBox.Show(DisplayName+" has Successfully logged in as a candidate !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //Display votes casting interface
                                    VotingGuide myobj = new VotingGuide();
                                    this.Hide();
                                    myobj.Show();
                                    break;

                                }
                                else
                                {
                                    label3.Text = "Finger Print is not matching. Access Denied !!";
                                    MessageBox.Show("Finger Print is not matching. Access Denied !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    

                                }


                            }
                        }
                        score = minutiaMatcher.Match(qFeatures, tFeatures, out matchingMtiae);  // matching algorithm


                        if (qFeatures is List<Minutia> && tFeatures is List<Minutia>)
                        {
                            pbxQueryImg.Image = qImage.Clone() as Bitmap;
                            Graphics g1 = Graphics.FromImage(pbxQueryImg.Image);
                            ShowBlueMinutiae(qFeatures as List<Minutia>, g1);


                            pictureBox1.Image = tImage.Clone() as Bitmap;
                            Graphics g2 = Graphics.FromImage(pictureBox1.Image);
                            ShowBlueMinutiae(tFeatures as List<Minutia>, g2);


                            if (score == 0 || matchingMtiae == null)
                                MessageBox.Show(string.Format("Similarity: {0}.", score));
                            else
                            {
                                List<Minutia> qMtiae = new List<Minutia>();
                                List<Minutia> tMtiae = new List<Minutia>();
                                foreach (MinutiaPair mPair in matchingMtiae)
                                {
                                    qMtiae.Add(mPair.QueryMtia);
                                    tMtiae.Add(mPair.TemplateMtia);
                                }
                                IFeatureDisplay<List<Minutia>> display = new MinutiaeDisplay();

                                display.Show(qMtiae, g1);
                                pbxQueryImg.Invalidate();

                                display.Show(tMtiae, g2);
                                pictureBox1.Invalidate();

                                MessageBox.Show(string.Format("Similarity: {0}. Matching minutiae: {1}.", score,
                                                              matchingMtiae.Count));
                            }
                        }
                        else
                            ShowResults(score, matchingMtiae);

                    }
                    else
                        score = matcher.Match(qFeatures, tFeatures);




                }
            }
            else
            {
                MessageBox.Show("Invalid User name !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void buttonLoadImg_Click(object sender, EventArgs e)
        {
            if (textBoxUname.Text == string.Empty) //To check whether the fields are empty
            {
                MessageBox.Show("Enter the User name !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxUname.Focus();
            }

            else if (comboBoxUsertype.Text == "Administrator")
            {
                DBLayer.Login obj = new DBLayer.Login(); //To access Login class properties 
                MySqlDataReader reader = obj.getAdminLogin(textBoxUname.Text);//To check whether username is valid
                if (reader.Read())
                {
                    DBLayer.DBConnection obj1 = new DBLayer.DBConnection();
                    MySqlCommand da = new MySqlCommand("select count(*) from administrator", obj1.createConnection());
                    int count = Convert.ToInt32(da.ExecuteScalar());


                    if (count > 0)
                    {
                        fpreg.FPRegistrationStart("MySecretKey" + textBoxUname.Text);// Start loading finger print to image box
                        openFileDialog1.InitialDirectory = resourcePath; // open the windows file to load the image
                        if (openFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            string shortFileName = Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
                            try
                            {
                                qFeatures = provider.GetResource(shortFileName, repository);//trying to load features of the loaded image using the repository
                            }
                            catch (Exception)
                            {
                                //error message of the features are not loaded
                                MessageBox.Show("Features not loaded !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                              
                                return;
                            }
                            qImage = ImageLoader.LoadImage(openFileDialog1.FileName);//set the qImage equal to the loaded image
                            pbxQueryImg.Image = qImage;// Equal picturebox image to qImage
                        }


                    }

                }
                else
                {
                    //error message inf the administrator username not in the database
                    MessageBox.Show("Invalid User Name. You are not an Administrator !!", " Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBoxUname.Text = String.Empty;
                    textBoxUname.Focus();
                }
                
            }
            
                else if (comboBoxUsertype.Text == "Voter")
                {
                    DBLayer.Login obj = new DBLayer.Login(); //To access Login class properties 
                    MySqlDataReader reader = obj.getVoterLogin(textBoxUname.Text);//To check whether username is valid
                    if (reader.Read())
                    {
                        DBLayer.DBConnection obj1 = new DBLayer.DBConnection();
                        MySqlCommand da = new MySqlCommand("select count(*) from voter", obj1.createConnection());
                        int count = Convert.ToInt32(da.ExecuteScalar());


                        if (count > 0)
                        {
                            fpreg.FPRegistrationStart("MySecretKey" + textBoxUname.Text);// Start loading finger print to image box
                            openFileDialog1.InitialDirectory = resourcePath;// open the windows file to load the image
                            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                            {
                                string shortFileName = Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
                                try
                                {
                                    qFeatures = provider.GetResource(shortFileName, repository);//trying to load features of the loaded image using the repository
                                }
                                catch (Exception)
                                {
                                    //error message of the features are not loaded
                                    MessageBox.Show("Finger Print is not matching. Access Denied !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }
                                qImage = ImageLoader.LoadImage(openFileDialog1.FileName);//set the qImage equal to the loaded image
                                pbxQueryImg.Image = qImage;// Equal picturebox image to qImage
                            }
                          

                        }
                     
                    }
                    else
                    {
                        //error message inf the voter username not in the database
                        MessageBox.Show("Invalid User Name. You are not a Registered Voter !!", " Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBoxUname.Text = String.Empty;
                        textBoxUname.Focus();
                    }
                }
                else
                {
                    DBLayer.Login obj = new DBLayer.Login(); //To access Login class properties 
                    MySqlDataReader reader1 = obj.getCandidateLogin(textBoxUname.Text);//To check whether username is valid
                    if (reader1.Read())
                    {
                        DBLayer.DBConnection obj2 = new DBLayer.DBConnection();
                        MySqlCommand da = new MySqlCommand("select count(*) from candidate", obj2.createConnection());
                        int count = Convert.ToInt32(da.ExecuteScalar());


                        if (count > 0)
                        {
                            fpreg.FPRegistrationStart("MySecretKey" + textBoxUname.Text);// Start loading finger print to image box
                            openFileDialog1.InitialDirectory = resourcePath;// open the windows file to load the image
                            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                            {
                                string shortFileName = Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
                                try
                                {
                                    qFeatures = provider.GetResource(shortFileName, repository);//trying to load features of the loaded image using the repository
                                }
                                catch (Exception)
                                {
                                    //error message of the features are not loaded
                                    MessageBox.Show("Finger Print is not matching. Access Denied !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }
                                qImage = ImageLoader.LoadImage(openFileDialog1.FileName);//set the qImage equal to the loaded image
                                pbxQueryImg.Image = qImage;// Equal picturebox image to qImage
                            }
                          
                        }
                      
                    }
                    else
                    {
                        //error message inf the candidate username not in the database
                        MessageBox.Show("Invalid User Name. You are not a Registered Candidate !!", " Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBoxUname.Text=String.Empty;
                        textBoxUname.Focus();
                    }
                }
             
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            Welcome obj = new Welcome();
            this.Hide();
            obj.Show();

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLayer;
using MySql.Data.MySqlClient;
using NUnit.Framework;
namespace DBLayer.NUnitTests
{
    [TestFixture()]
    public class AdminTest
    {
        [Test()]
        public void getAdmininfoTest()
        {
            //Expected values
            String expectedFirstName = "New Kasun";
            String expectedLastName = "New Eranga";
            String expectedGender = "male";
            String expectedNIC = "222222222V";
            String expectedEmail = "foo@bar.com";
            String expectedContNo = "123456789";
            String expectedUsername = "kasunroxx";

            Admin retreiveAdmin = new Admin();
            MySqlDataReader rd = retreiveAdmin.getAdmininfo("1");

            while (rd.Read())//while loop to retrieve voter information
            {
                String OutPutFName = (String)rd["AFname"];
                String OutPutLName = (String)rd["ALname"];
                String OutPutGender = (String)rd["AGender"];
                String OutPutNIC = (String)rd["ANIC"];
                String OutPutEmail = (String)rd["AEmail"];
                String OutPutContNo = (String)rd["AContNo"];
                String OutPutUsername = (String)rd["AUsername"];

                //check whether the expected value is equal to the output value
                Assert.AreEqual(expectedFirstName, OutPutFName);
                Assert.AreEqual(expectedLastName, OutPutLName);
                Assert.AreEqual(expectedGender, OutPutGender);
                Assert.AreEqual(expectedNIC, OutPutNIC);
                Assert.AreEqual(expectedEmail, OutPutEmail);
                Assert.AreEqual(expectedContNo, OutPutContNo);
                Assert.AreEqual(expectedUsername, OutPutUsername);
            }
        }

        [Test()]
        public void UpdateAdminTest()
        {
             DateTime methodStartTime = DateTime.Now;
            
            //Parameters
            Admin obj = new Admin();
            obj.AID1 = "1";
            obj.AFname1 = "New Kasun";
            obj.ALname1 = "New Eranga";
            obj.AGender1 = "male";
            obj.ANIC1 = "222222222V";
            obj.AEmail1 = "foo@bar.com";
            obj.AContNo1 = "123456789";
            obj.AUname1 = "kasunroxx";
            obj.UpdateAdmin(obj);
            TimeSpan methodDuration = DateTime.Now.Subtract(methodStartTime);
            Console.WriteLine(String.Format("Administrator Updated Succesfully !!"));
            Console.WriteLine(String.Format("DBLayer.Voter.UpdateAdmin Time Elapsed: {0}", methodDuration));

            //Expected values
            String expectedFirstName = "New Kasun";
            String expectedLastName = "New Eranga";
            String expectedGender = "male";
            String expectedNIC = "222222222V";
            String expectedEmail = "foo@bar.com";
            String expectedContNo = "123456789";
            String expectedUsername = "kasunroxx";

            Admin retreiveAdmin = new Admin();
            MySqlDataReader rd = retreiveAdmin.getAdmininfo("1");

            while (rd.Read())//while loop to retrieve voter information
            {
                String OutPutFName = (String)rd["AFname"];
                String OutPutLName = (String)rd["ALname"];
                String OutPutGender = (String)rd["AGender"];
                String OutPutNIC = (String)rd["ANIC"];
                String OutPutEmail = (String)rd["AEmail"];
                String OutPutContNo = (String)rd["AContNo"];
                String OutPutUsername = (String)rd["AUsername"];

                //check whether the expected value is equal to the output value
                Assert.AreEqual(expectedFirstName, OutPutFName);
                Assert.AreEqual(expectedLastName, OutPutLName);
                Assert.AreEqual(expectedGender, OutPutGender);
                Assert.AreEqual(expectedNIC, OutPutNIC);
                Assert.AreEqual(expectedEmail, OutPutEmail); 
                Assert.AreEqual(expectedContNo, OutPutContNo);
                Assert.AreEqual(expectedUsername, OutPutUsername);
                
            }
           
        }

        [Test()]
        public void DeleteAdminTest()
        {
            DateTime methodStartTime = DateTime.Now;

            //Parameters
            Admin obj = new Admin();
            obj.AID1 = "1";
            obj.DeleteAdmin(obj);
            Console.WriteLine(String.Format("Admin Removed Succesfully !!"));
            TimeSpan methodDuration = DateTime.Now.Subtract(methodStartTime);
            Console.WriteLine(String.Format("DBLayer.Voter.DeleteAdmin Time Elapsed: {0}", methodDuration));

            String ExpectedAID = null;

            Admin retreiveVoter = new Admin();
            MySqlDataReader rd = retreiveVoter.getAdmininfo("1");
            if (rd == null)
            {
                String OutputAID = null;
                Assert.AreEqual(ExpectedAID, OutputAID);
            }
        }
    }
}

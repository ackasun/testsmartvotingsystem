﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLayer;
using NUnit.Framework;
using MySql.Data.MySqlClient;
namespace DBLayer.NUnitTests
{
    [TestFixture()]
    public class PartyTest
    {
        [Test()]
        public void insertPartyTest()
        {
            DateTime methodStartTime = DateTime.Now;

            //Parameters
            Party obj = new Party();
            obj.PID1 = "1";
            obj.Pname1 = "Batch 57";
            obj.PLeader1 = "Kasun Amarasinghe";
            obj.NoOfCandidates1 = "4";

            obj.insertParty(obj);
            TimeSpan methodDuration = DateTime.Now.Subtract(methodStartTime);
            Console.WriteLine(String.Format("Party added Succesfully !!"));
            Console.WriteLine(String.Format("DBLayer.Voter. Time Elapsed: {0}", methodDuration));

            //Expected values
            String expectedPartyName = "Batch 57";
            String expectedPartyLeader = "Kasun Amarasinghe";
            String expectedNoOfCandidates = "4";


            Party retreiveParty = new Party();
            MySqlDataReader rd = retreiveParty.getPartyinfo("1");

            while (rd.Read())//while loop to retrieve voter information
            {
                String OutPutPartyName = (String)rd["Pname"];
                String OutPutPartyLeader = (String)rd["PLeader"];
                String OutPutNoOfCandidates = (String)rd["NoOfCandidates"];

                //check whether the expected value is equal to the output value
                Assert.AreEqual(expectedPartyName, OutPutPartyName);
                Assert.AreEqual(expectedPartyLeader, OutPutPartyLeader);
                Assert.AreEqual(expectedNoOfCandidates, OutPutNoOfCandidates);
            } 
        }

        [Test()]
        public void getPartyinfoTest()
        {
              //Expected values
                String expectedPartyName = "Batch 57";
                String expectedPartyLeader = "Kasun Amarasinghe";
                String expectedNoOfCandidates = "4";
                

                Party retreiveParty = new Party();
                MySqlDataReader rd = retreiveParty.getPartyinfo("1");

                while (rd.Read())//while loop to retrieve voter information
                {
                    String OutPutPartyName = (String)rd["Pname"];
                    String OutPutPartyLeader = (String)rd["PLeader"];
                    String OutPutNoOfCandidates = (String)rd["NoOfCandidates"];

                    //check whether the expected value is equal to the output value
                    Assert.AreEqual(expectedPartyName, OutPutPartyName);
                    Assert.AreEqual(expectedPartyLeader, OutPutPartyLeader);
                    Assert.AreEqual(expectedNoOfCandidates, OutPutNoOfCandidates);
                }
        }

        [Test()]
        public void UpdatePartyTest()
        {
            DateTime methodStartTime = DateTime.Now;

            //Parameters
            Party obj = new Party();
            obj.PID1 = "P001";
            obj.Pname1 = "Batch 57";
            obj.PLeader1 = "Kasun Amarasinghe";
            obj.NoOfCandidates1 = "4";
            
            obj.UpdateParty(obj);
            TimeSpan methodDuration = DateTime.Now.Subtract(methodStartTime);
            Console.WriteLine(String.Format("Party Updated Succesfully !!"));
            Console.WriteLine(String.Format("DBLayer.Voter.UpdateVoter Time Elapsed: {0}", methodDuration));

            //Expected values
            String expectedPartyName = "Batch 57";
            String expectedPartyLeader = "Kasun Amarasinghe";
            String expectedNoOfCandidates = "4";


            Party retreiveParty = new Party();
            MySqlDataReader rd = retreiveParty.getPartyinfo("P001");

            while (rd.Read())//while loop to retrieve voter information
            {
                String OutPutPartyName = (String)rd["Pname"];
                String OutPutPartyLeader = (String)rd["PLeader"];
                String OutPutNoOfCandidates = (String)rd["NoOfCandidates"];

                //check whether the expected value is equal to the output value
                Assert.AreEqual(expectedPartyName, OutPutPartyName);
                Assert.AreEqual(expectedPartyLeader, OutPutPartyLeader);
                Assert.AreEqual(expectedNoOfCandidates, OutPutNoOfCandidates);
            }
        }

        [Test()]
        public void DeletePartyTest()
        {
            DateTime methodStartTime = DateTime.Now;

            //Parameters
            Party obj = new Party();
            obj.PID1 = "1";
            obj.DeleteParty(obj);
            Console.WriteLine(String.Format("Party Removed Succesfully !!"));
            TimeSpan methodDuration = DateTime.Now.Subtract(methodStartTime);
            Console.WriteLine(String.Format("DBLayer.Voter.DeleteParty Time Elapsed: {0}", methodDuration));

            String ExpectedPID = null;

            Party retreiveVoter = new Party();
            MySqlDataReader rd = retreiveVoter.getPartyinfo("P001");
            if (rd == null)
            {
                String OutputPID = null;
                Assert.AreEqual(ExpectedPID, OutputPID);
            }
        }
    }
}

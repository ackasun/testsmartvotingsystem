﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLayer;
using NUnit.Framework;
using MySql.Data.MySqlClient;
namespace DBLayer.NUnitTests
{
    [TestFixture, Description("Tests for Candidate")]
    public class CandidateTest
    {
           [Test()]
           public void getCandidateInfoTest()
           {
               //Expected values
               String expectedFirstName = "kasun";
               String expectedGender = "male";
               String expectedNIC = "111111111v";
               String expectedEmail = "kasun@gmail.com";
               String expectedContNo = "2222222222";
               String expectedPost = "Prasident";
               String expectedPID = "1";
               String expectedPname = "batch08";
               String expectedUsername = "kasun";

               Candidate retreiveCandidate = new Candidate();
               MySqlDataReader rd = retreiveCandidate.getCandidateInfo("1");

               while (rd.Read())//while loop to retrieve voter information
               {
                   String OutPutFName = (String)rd["CFname"];
                   String OutPutGender = (String)rd["CGender"];
                   String OutPutNIC =    (String)rd["CNIC"];
                   String OutPutEmail = (String)rd["CEmail"];
                   String OutPutContNo = (String)rd["CContNo"];
                   String OutPutPost = (String)rd["Post"];
                   String OutPutPID=   (String)rd["PID"];
                   String OutPutPname = (String)rd["Pname"];
                   String OutPutUsername = (String)rd["Username"];

                   //check whether the expected value is equal to the output value
                   Assert.AreEqual(expectedFirstName, OutPutFName);
                   Assert.AreEqual(expectedGender, OutPutGender);
                   Assert.AreEqual(expectedNIC, OutPutNIC);
                   Assert.AreEqual(expectedEmail, OutPutEmail);
                   Assert.AreEqual(expectedContNo, OutPutContNo);
                   Assert.AreEqual(expectedPost, OutPutPost);
                   Assert.AreEqual(expectedPID, OutPutPID);
                   Assert.AreEqual(expectedPname, OutPutPname);
                   Assert.AreEqual(expectedUsername, OutPutUsername);
               }
           }

        /// Update Candidate Method Test
        [Test()]
        public void UpdateCandidateTest()
        {
            DateTime methodStartTime = DateTime.Now;

            //Parameters
            Candidate obj = new Candidate();
            obj.CID1 = "1";
            obj.CFname1 = "kasun";
            obj.CGender1 = "male";
            obj.CNIC1 = "111111111v";
            obj.CEmail1 = "kasun@gmail.com";
            obj.CContNo1 = "2222222222";
            obj.PID1 = "1";
            obj.Pname1 = "batch08";
            obj.Uname1 = "kasun";
            obj.Post1 = "Prasident";
            obj.UpdateCandidate(obj);
            TimeSpan methodDuration = DateTime.Now.Subtract(methodStartTime);
            Console.WriteLine(String.Format("Candidate Updated Succesfully !!"));
            Console.WriteLine(String.Format("DBLayer.Candidate.UpdateCandidate Time Elapsed: {0}", methodDuration));

             //Expected values
               String expectedFirstName = "kasun";
               String expectedGender = "male";
               String expectedNIC = "111111111v";
               String expectedEmail = "kasun@gmail.com";
               String expectedContNo = "2222222222";
               String expectedPost = "Prasident";
               String expectedPID = "1";
               String expectedPname = "batch08";
               String expectedUsername = "kasun";

               Candidate retreiveCandidate = new Candidate();
               MySqlDataReader rd = retreiveCandidate.getCandidateInfo("1");

               while (rd.Read())//while loop to retrieve candidate information
               {
                   String OutPutFName = (String)rd["CFname"];
                   String OutPutGender = (String)rd["CGender"];
                   String OutPutNIC = (String)rd["CNIC"];
                   String OutPutEmail = (String)rd["CEmail"];
                   String OutPutContNo = (String)rd["cContNo"];
                   String OutPutPost = (String)rd["Post"];
                   String OutPutPID = (String)rd["PID"];
                   String OutPutPname = (String)rd["Pname"];
                   String OutPutUsername = (String)rd["Username"];

                   //check whether the expected value is equal to the output value
                   Assert.AreEqual(expectedFirstName, OutPutFName);
                   Assert.AreEqual(expectedGender, OutPutGender);
                   Assert.AreEqual(expectedNIC, OutPutNIC);
                   Assert.AreEqual(expectedEmail, OutPutEmail);
                   Assert.AreEqual(expectedContNo, OutPutContNo);
                   Assert.AreEqual(expectedPost, OutPutPost);
                   Assert.AreEqual(expectedPID, OutPutPID);
                   Assert.AreEqual(expectedPname, OutPutPname);
                   Assert.AreEqual(expectedUsername, OutPutUsername);
               }
           
        }

        /// Delete Candidate Method Test
        [Test()]
        public void DeleteCandidateTest()
        {
            DateTime methodStartTime = DateTime.Now;

            //Parameters
            Voter obj = new Voter();
            obj.VID1 = "1";
            obj.DeleteVoter(obj);
            Console.WriteLine(String.Format("Candidate Removed Succesfully !!"));

            TimeSpan methodDuration = DateTime.Now.Subtract(methodStartTime);
            Console.WriteLine(String.Format("DBLayer.Voter.DeleteVoter Time Elapsed: {0}", methodDuration));
        }

    }
}

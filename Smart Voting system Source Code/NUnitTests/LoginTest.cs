﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLayer;
using MySql.Data.MySqlClient;
using NUnit.Framework;
namespace DBLayer.NUnitTests
{
    [TestFixture()]
    public class LoginTest
    {
        [Test()]
        public void getAdminLoginTest()
        {
             String expectedUsername = "Admin";

            Login retreiveAdmin = new Login();
            MySqlDataReader rd = retreiveAdmin.getAdminLogin("Admin");

            while (rd.Read())//while loop to retrieve voter information
            {

                String OutPutUsername = (String)rd["AUsername"];

                //check whether the expected value is equal to the output value
                Assert.AreEqual(expectedUsername, OutPutUsername);
            }
        }

        [Test()]
        public void getVoterLoginTest()
        {
            String expectedUsername = "kasunroxx";

            Login retreiveVoter = new Login();
            MySqlDataReader rd = retreiveVoter.getVoterLogin("kasunroxx");

            while (rd.Read())//while loop to retrieve voter information
            {

                String OutPutUsername = (String)rd["Username"];

                //check whether the expected value is equal to the output value
                Assert.AreEqual(expectedUsername, OutPutUsername);
            }
        }

        [Test()]
        public void getCandidateLoginTest()
        {
            String expectedUsername = "kasun12";

            Login retreivecandi = new Login();
            MySqlDataReader rd = retreivecandi.getCandidateLogin("kasun12");

            while (rd.Read())//while loop to retrieve voter information
            {

                String OutPutUsername = (String)rd["Username"];

                //check whether the expected value is equal to the output value
                Assert.AreEqual(expectedUsername, OutPutUsername);
            }
        }
    }
}

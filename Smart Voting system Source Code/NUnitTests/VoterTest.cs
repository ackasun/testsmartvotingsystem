﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLayer;
using NUnit.Framework;
using MySql.Data.MySqlClient;
namespace DBLayer.NUnitTests
{
    /// <summary>
    /// Tests for the Voter Class
    /// Documentation: 
    /// </summary>
    [TestFixture, Description("Tests for Voter")]
    public class VoterTest
    {
            /// get Voterinfo Method Test

            [Test]
            public void getVoterinfoTest()
            {
                //Expected values
                String expectedFirstName = "New Kasun";
                String expectedLastName = "New Eranga";
                String expectedGender = "male";
                String expectedNIC = "222222222V";
                String expectedEmail = "foo@bar.com";
                String expectedContNo = "123456789";
                String expectedUsername = "kasunroxx";

                Voter retreiveVoter = new Voter();
                MySqlDataReader rd = retreiveVoter.getVoterinfo("1");

                while (rd.Read())//while loop to retrieve voter information
                {
                    String OutPutFName = (String)rd["Fname"];
                    String OutPutLName = (String)rd["Lname"];
                    String OutPutGender = (String)rd["Gender"];
                    String OutPutNIC = (String)rd["NIC"];
                    String OutPutEmail = (String)rd["Email"];
                    String OutPutContNo = (String)rd["ContNo"];
                    String OutPutUsername = (String)rd["Username"];

                    //check whether the expected value is equal to the output value
                    Assert.AreEqual(expectedFirstName, OutPutFName);
                    Assert.AreEqual(expectedLastName, OutPutLName);
                    Assert.AreEqual(expectedGender, OutPutGender);
                    Assert.AreEqual(expectedNIC, OutPutNIC);
                    Assert.AreEqual(expectedEmail, OutPutEmail);
                    Assert.AreEqual(expectedContNo, OutPutContNo);
                    Assert.AreEqual(expectedUsername, OutPutUsername);
                }
            }

        /// Update Voter Method Test
        [Test]
        public void UpdateVoterTest()
        {
            DateTime methodStartTime = DateTime.Now;
            
            //Parameters
            Voter updateVoterTestInstance = new Voter();
            updateVoterTestInstance.VID1 = "1";
            updateVoterTestInstance.Fname1 = "New Kasun";
            updateVoterTestInstance.Lname1 = "New Eranga";
            updateVoterTestInstance.Gender1 = "male";
            updateVoterTestInstance.NIC1 = "222222222V";
            updateVoterTestInstance.Email1 = "foo@bar.com";
            updateVoterTestInstance.ContNo1 = "123456789";
            updateVoterTestInstance.Uname1 = "kasunroxx";
            updateVoterTestInstance.UpdateVoter(updateVoterTestInstance);
            TimeSpan methodDuration = DateTime.Now.Subtract(methodStartTime);
            Console.WriteLine(String.Format("Voter Updated Succesfully !!"));
            Console.WriteLine(String.Format("DBLayer.Voter.UpdateVoter Time Elapsed: {0}", methodDuration));

            //Expected values
            String expectedFirstName = "New Kasun";
            String expectedLastName = "New Eranga";
            String expectedGender = "male";
            String expectedNIC = "222222222V";
            String expectedEmail = "foo@bar.com";
            String expectedContNo = "123456789";
            String expectedUsername = "kasunroxx";

            Voter retreiveVoter = new Voter();
            MySqlDataReader rd = retreiveVoter.getVoterinfo("1");
            if (rd==null)

            while (rd.Read())//while loop to retrieve voter information
            {
                String OutPutFName = (String)rd["Fname"];
                String OutPutLName = (String)rd["Lname"];
                String OutPutGender = (String)rd["Gender"];
                String OutPutNIC = (String)rd["NIC"];
                String OutPutEmail = (String)rd["Email"];
                String OutPutContNo = (String)rd["ContNo"];
                String OutPutUsername = (String)rd["Username"];

                //check whether the expected value is equal to the output value
                Assert.AreEqual(expectedFirstName, OutPutFName);
                Assert.AreEqual(expectedLastName, OutPutLName);
                Assert.AreEqual(expectedGender, OutPutGender);
                Assert.AreEqual(expectedNIC, OutPutNIC);
                Assert.AreEqual(expectedEmail, OutPutEmail); 
                Assert.AreEqual(expectedContNo, OutPutContNo);
                Assert.AreEqual(expectedUsername, OutPutUsername);
                
            }  
        }

        /// Delete Voter Method Test
        [Test]
        public void DeleteVoterTest()
        {
            DateTime methodStartTime = DateTime.Now;

            //Parameters
            Voter obj = new Voter();
            obj.VID1 = "1";
            obj.DeleteVoter(obj);
            Console.WriteLine(String.Format("Voter Removed Succesfully !!"));
            TimeSpan methodDuration = DateTime.Now.Subtract(methodStartTime);
            Console.WriteLine(String.Format("DBLayer.Voter.DeleteVoter Time Elapsed: {0}", methodDuration));

            String ExpectedVID = null;

            Voter retreiveVoter = new Voter();
            MySqlDataReader rd = retreiveVoter.getVoterinfo("1");
            if (rd == null)
            {
                String OutputVID = null;
                Assert.AreEqual(ExpectedVID, OutputVID);
            }
        }
    }
}

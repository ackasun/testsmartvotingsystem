﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using DBLayer;
namespace BLLayer
{
   public class SendEmail
    {
        String Email;

        public String Email1
        {
            get { return Email; }
            set { Email = value; }
        }
        String CandidateName;

        public String CandidateName1
        {
            get { return CandidateName; }
            set { CandidateName = value; }
        }
        String Post;

        public String Post1
        {
            get { return Post; }
            set { Post = value; }
        }
        String TotalVotes;

        public String TotalVotes1
        {
            get { return TotalVotes; }
            set { TotalVotes = value; }
        }
        public void Sendmail(SendEmail obj)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();

                message.From = new MailAddress("kasun.ac.m1@gmail.com");
                String MailAddress = Email;
                message.To.Add(new MailAddress(MailAddress));
                message.Subject = "Election Results";
                message.Body = "Dear " + obj.CandidateName + ", Your Total votes calculation for the " + obj.Post + " post is " + obj.TotalVotes + " Votes. Thank You.";
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("kasun.ac.m1@gmail.com", "kasun123");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
               
            }
            catch (Exception ex)
            {
               
            }
        }
    }
}

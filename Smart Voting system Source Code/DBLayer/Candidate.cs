﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DBLayer
{
   public class Candidate
    {
        String CID;

        public String CID1
        {
            get { return CID; }
            set { CID = value; }
        }
        String CFname;

        public String CFname1
        {
            get { return CFname; }
            set { CFname = value; }
        }

        String CGender;

        public String CGender1
        {
            get { return CGender; }
            set { CGender = value; }
        }
        String CNIC;

        public String CNIC1
        {
            get { return CNIC; }
            set { CNIC = value; }
        }
        String CEmail;

        public String CEmail1
        {
            get { return CEmail; }
            set { CEmail = value; }
        }
        String CContNo;

        public String CContNo1
        {
            get { return CContNo; }
            set { CContNo = value; }
        }
        String PID;

        public String PID1
        {
            get { return PID; }
            set { PID = value; }
        }
        String Pname;

        public String Pname1
        {
            get { return Pname; }
            set { Pname = value; }
        }
        String Uname;

        public String Uname1
        {
            get { return Uname; }
            set { Uname = value; }
        }
        String Post;

        public String Post1
        {
            get { return Post; }
            set { Post = value; }
        }

        public MySqlDataReader getCandidateInfo(String Number)//method to retrieve user information from the database
        {
            //DBConnection object

            //sql query to retrieve user information from the database
            String SQL = "SELECT * FROM candidate where CID='" + Number + "'  ";
            DBConnection myobj = new DBConnection();
            return myobj.getdata(SQL);
        }
        public void UpdateCandidate(Candidate obj)
        {
            String SQL = "Update candidate set CFname='" + obj.CFname + "', CGender='"+ obj.CGender + "', CNIC='" + obj.CNIC + "', CEmail='" + obj.CEmail + "', CContNo='" + obj.CContNo + "', Post='" + obj.Post + "', PID='" + obj.PID + "', Pname='" + obj.Pname + "', Username='" + obj.Uname + "' where CID='" + obj.CID + "'";

            DBConnection objcon = new DBConnection(); //to access properties of DBConnection class 
            objcon.addvalues(SQL);

        }
        public void DeleteCandidate(Candidate obj)
        {

            String SQL = "DELETE FROM candidate where CID='" + obj.CID + "'";

            DBConnection objcon = new DBConnection(); //to access properties of DBConnection class 
            objcon.addvalues(SQL);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace DBLayer
{
    public class Admin
    {
        String AID;

        public String AID1
        {
            get { return AID; }
            set { AID = value; }
        }
        String AFname;

        public String AFname1
        {
            get { return AFname; }
            set { AFname = value; }
        }
        String ALname;

        public String ALname1
        {
            get { return ALname; }
            set { ALname = value; }
        }
        String AGender;

        public String AGender1
        {
            get { return AGender; }
            set { AGender = value; }
        }
        String ANIC;

        public String ANIC1
        {
            get { return ANIC; }
            set { ANIC = value; }
        }
        String AEmail;

        public String AEmail1
        {
            get { return AEmail; }
            set { AEmail = value; }
        }
        String AContNo;

        public String AContNo1
        {
            get { return AContNo; }
            set { AContNo = value; }
        }
        String AUname;

        public String AUname1
        {
            get { return AUname; }
            set { AUname = value; }
        }


        public MySqlDataReader getAdmininfo(String Number)//method to retrieve user information from the database
        {
            //DBConnection object

            //sql query to retrieve user information from the database
            String SQL = "SELECT * FROM administrator where AID='" + Number + "'  ";
            DBConnection myobj = new DBConnection();
            return myobj.getdata(SQL);
        }
        public void UpdateAdmin(Admin obj)
        {
            String SQL = "Update administrator set AFname='" + obj.AFname + "', ALname='" + obj.ALname + "', AGender='"
               + obj.AGender + "', ANIC='" + obj.ANIC + "', AEmail='" + obj.AEmail + "', AContNo='" + obj.AContNo + "', AUsername='" + obj.AUname + "' where AID='" + obj.AID + "'";

            DBConnection objcon = new DBConnection(); //to access properties of DBConnection class 
            objcon.addvalues(SQL);

        }
        public void DeleteAdmin(Admin obj)
        {

            String SQL = "DELETE FROM administrator where AID='" + obj.AID + "'";

            DBConnection objcon = new DBConnection(); //to access properties of DBConnection class 
            objcon.addvalues(SQL);
        }

    }
}

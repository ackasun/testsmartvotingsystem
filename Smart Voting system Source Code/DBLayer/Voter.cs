﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DBLayer
{
   public class Voter
    {
        String VID;

        public String VID1
        {
            get { return VID; }
            set { VID = value; }
        }
        String Fname;

        public String Fname1
        {
            get { return Fname; }
            set { Fname = value; }
        }
        String Lname;

        public String Lname1
        {
            get { return Lname; }
            set { Lname = value; }
        }
        String Gender;

        public String Gender1
        {
            get { return Gender; }
            set { Gender = value; }
        }
        String NIC;

        public String NIC1
        {
            get { return NIC; }
            set { NIC = value; }
        }
        String Email;

        public String Email1
        {
            get { return Email; }
            set { Email = value; }
        }
        String ContNo;

        public String ContNo1
        {
            get { return ContNo; }
            set { ContNo = value; }
        }
        String Uname;

        public String Uname1
        {
            get { return Uname; }
            set { Uname = value; }
        }
        public MySqlDataReader getVoterinfo(String Number)//method to retrieve user information from the database
        {
            //DBConnection object
            
            //sql query to retrieve user information from the database
            String SQL = "SELECT * FROM voter where VID='" +Number + "'  ";
            DBConnection myobj = new DBConnection();
            return myobj.getdata(SQL);
        }
        public void UpdateVoter(Voter obj)
        
        {
            String SQL = "Update voter set Fname='" + obj.Fname + "', Lname='" + obj.Lname + "', Gender='"
               + obj.Gender + "', NIC='" + obj.NIC + "', Email='" + obj.Email + "', ContNo='" + obj.ContNo + "', Username='" + obj.Uname + "' where VID='"+obj.VID+"'";

            DBConnection objcon = new DBConnection(); //to access properties of DBConnection class 
            objcon.addvalues(SQL);

        }
        public void DeleteVoter(Voter obj)
        {

            String SQL = "DELETE FROM voter where VID='" + obj.VID + "'";

            DBConnection objcon = new DBConnection(); //to access properties of DBConnection class 
            objcon.addvalues(SQL);
        }

    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLayer;
using MySql.Data.MySqlClient;

namespace DBLayer
{
 public class Party
    {
        String PID;

        public String PID1
        {
            get { return PID; }
            set { PID = value; }
        }
        String Pname;

        public String Pname1
        {
            get { return Pname; }
            set { Pname = value; }
        }
        String PLeader;

        public String PLeader1
        {
            get { return PLeader; }
            set { PLeader = value; }
        }
        String NoOfCandidates;

        public String NoOfCandidates1
        {
            get { return NoOfCandidates; }
            set { NoOfCandidates = value; }
        }
        byte[] imageSymbol;

        public byte[] ImageSymbol
        {
            get { return imageSymbol; }
            set { imageSymbol = value; }
        }
        public void insertParty(Party obj) //SQL statement for save data 
        {
            String SQL = "insert into party values ('" + obj.PID + "', '" + obj.Pname + "', '" + obj.PLeader + "', '"
               + obj.NoOfCandidates + "', '" + obj.imageSymbol + "') ";

            DBConnection objcon = new DBConnection(); //to access properties of DBConnection class 
            objcon.addvalues(SQL);
        }

        public MySqlDataReader getPartyinfo(String Number)//method to retrieve user information from the database
        {
            //DBConnection object

            //sql query to retrieve user information from the database
            String SQL = "SELECT * FROM party where PID='" + Number + "'  ";
            DBConnection myobj = new DBConnection();
            return myobj.getdata(SQL);
        }
        public void UpdateParty(Party obj)
        {
            String SQL = "Update party set Pname='" + obj.Pname + "', PLeader='" + obj.PLeader + "', NoOfCandidates='"
               + obj.NoOfCandidates + "' where PID='" + obj.PID + "'";

            DBConnection objcon = new DBConnection(); //to access properties of DBConnection class 
            objcon.addvalues(SQL);

        }
        public void DeleteParty(Party obj)
        {

            String SQL = "DELETE FROM party where PID='" + obj.PID + "'";

            DBConnection objcon = new DBConnection(); //to access properties of DBConnection class 
            objcon.addvalues(SQL);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DBLayer
{
    public class Login
    {
        String Username;

        public String Username1
        {
            get { return Username; }
            set { Username = value; }
        }

        public MySqlDataReader getVoterLogin(String Uname)
        {
            String SQL = "SELECT * FROM voter where Username='" + Uname + "' ";

            DBConnection myobj = new DBConnection();
            return myobj.getdata(SQL);
        }
        public MySqlDataReader getCandidateLogin(String Uname)
        {
            String SQL = "SELECT * FROM candidate where Username='" + Uname + "' ";
          
            DBConnection myobj = new DBConnection();
            return myobj.getdata(SQL);
        }
        public MySqlDataReader getAdminLogin(String Uname)
        {
            String SQL = "SELECT * FROM administrator where AUsername='" + Uname + "' ";

            DBConnection myobj = new DBConnection();
            return myobj.getdata(SQL);
        }
    }
}

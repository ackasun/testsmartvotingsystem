﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Diagnostics;


namespace DBLayer
{

    public class DBConnection//DBConnection class
    {
        private MySqlConnection connection;

        public MySqlConnection createConnection()
        {
            connection = new MySqlConnection(@"server=localhost;database=icbtelection;username=root;password=;");
            try
            {
                connection.Open();
                return connection;
            }
            catch (Exception)
            {
                connection = null;
                return connection;
            }

        }


        public void addvalues(string Sql) //to insert and update data in database 
        {
            connection = createConnection();
            MySqlCommand cmd = new MySqlCommand(Sql, connection);
            int i = cmd.ExecuteNonQuery();
        }

        public MySqlDataReader getdata(string sql) //to search and delete data in database 
        {
            connection = createConnection();
            MySqlCommand mycom = new MySqlCommand(sql, connection);

            MySqlDataReader DataReader = mycom.ExecuteReader(); //execure the reader

            return DataReader;
        }

    }
}
